package cz.cvut.srotypet;

import cz.cvut.srotypet.model.Administrator;
import cz.cvut.srotypet.model.User;

import java.util.ArrayList;

public class Playground {

    private ArrayList<User> users = new ArrayList<>();

        public void play(){
            typeAssigment();
        }

        public void typeAssigment(){
            User user0 = new Administrator("A","B","AB","A");
            User user1 = new User("B","C","BC");
            User user2 = new Administrator("C","B","CB","B");
            User user3 = new User("D","E","DE");
            User user4 = new Administrator("E","F","EF","C");
            User user5 = new User("F","G","FG");
            User user6 = new Administrator("G","H","GH","D");
            User user7 = new User("H","I","HI");
            User user8 = new Administrator("I","J","IJ","E");
            User user9 = new User("J","K","JK");
            registerUsers(user0);
            registerUsers(user1);
            registerUsers(user2);
            registerUsers(user3);
            registerUsers(user4);
            registerUsers(user5);
            registerUsers(user6);
            registerUsers(user7);
            registerUsers(user8);
            registerUsers(user9);

            for (User user:users) {
                user.pay();
            }

            System.out.println("\nplatby pres instance of: ");
            makeUsersPajet(users);

            /*int i = 5;
            float k = 5;
            float o=k+i;
            */
        }

        public void registerUsers(User user){
            users.add(user);
        }

        public void makeUsersPajet(ArrayList<User> userList){
            for (User user:users) {
                if (user instanceof Administrator ) {
                    ((Administrator) user).pay();
                }
            }
        }
}


