package cz.cvut.srotypet.model;

public class User {
    private String name;
    private String surname;
    private String username;
    private String password;

    public User(){ System.out.println("Uzivatel vytvoren"); }

    public User(String name, String surname, String username){
        this.name = name;
        this.surname = surname;
        this.username = username;
    }

    public void registration(){ System.out.println("Uzivatel registrovan"); }

    public void login(){ System.out.println("uzivatel prihlasen"); }

    public void logout(){ System.out.println("uzivatel odhlasen"); }

    public void addToCart(){ System.out.println("ponozka pridana do kosiku"); }

    public String getName() { return name; }

    //public void setName(String name) { this.name = name; }

    public String getSurname() { return surname; }

    //public void setSurname(String surname) { this.surname = surname; }

    public String getUsername() { return username; }

    //public void setUsername(String username) { this.username = username; }

    public void pay(){
        System.out.println(getUsername() + " zaplaceno");
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
