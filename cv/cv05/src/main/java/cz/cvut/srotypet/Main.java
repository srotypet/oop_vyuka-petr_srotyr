package cz.cvut.srotypet;

public class Main {

    public static void main(String[] args) {
        Playground play = new Playground();
        play.play();
        Calc calc = new Calc();
        System.out.println("\n\n Sumarni kalkulacka");
        System.out.println("==============================");
        int i=10; float j=10;
        System.out.print("suma int + float: ");
        System.out.println(calc.sum(i,j));
        System.out.print("suma float + int: ");
        System.out.println(calc.sum(j,i));
        System.out.print("suma tri intu: ");
        System.out.println(calc.sum(10,10,10));
        //Pri scitani int + float fce vraci vysledek ve floatu a pri float + int take ve floatu.
        //pokud do funkce zadam ciste 10 a 10 (calc.cum(10,10) tak program vyhodi chybu jelikoz nevi, ktera funkce v Calc ma byt pouzita (Overloadnig)
    }
}
