package cz.cvut.srotypet.model;

public class Administrator extends User {

    private String department;
    private String room;

    public Administrator(){ System.out.println("Admin vytvoren"); };

    public Administrator(String name, String surname, String username, String department){
        super(name, surname, username);
        this.department = department;
    }

    public void deleteUser(){
        System.out.println("uzivatel byl smazan");
    }

    public void changePassword(){
        System.out.println("uzivateli bylo zmeneno heslo");
    }

    public void confirmAccount(){
        System.out.println("Ucet uzivatele byl potvrzen");
    }

    @Override
    public void pay() {
        System.out.println(getUsername() + " zaplaceno se slevou");
    }

    @Override
    public String toString() {
        return "Administrator{" + getName()+ " " + getSurname() + "  " +
                "department='" + department + '\'' +
                ", room='" + room + '\'' +
                '}';
    }
}
