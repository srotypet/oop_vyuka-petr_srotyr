package cz.cvut.srotypet;

public class Calc {
    public float sum(int i, float j){
        float sum = i+j;
        return  sum;
    }
    public float sum(float i, int j){
        float sum = i+j;
        return  sum;
    }
    public int sum(int i, int j, int k){
        int sum = i+j+k;
        return sum;
    }
}
