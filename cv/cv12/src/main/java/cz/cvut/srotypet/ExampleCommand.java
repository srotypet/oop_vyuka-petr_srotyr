package cz.cvut.srotypet;

public class ExampleCommand {
    private String value;
    private StringBuilder builder;

    public ExampleCommand(StringBuilder builder, String value){
        this.builder = builder;
        this.value = value;
    }
    public void execute(){
        builder.append(value);
    }

    public void undo(){
        String temp = builder.toString().replace(value,"");
        builder = new StringBuilder(temp);
    }

    public String getValue(){
        return value;
    }
}
