package cz.cvut.srotypet;

public class Patient {
    public String IDpac;
    private String name;
    private String surname;
    private Integer id = 0;
    private Illness medication;

    public Patient(String name, String surname){
        String idpac = getID();
        medication.medIdPac(idpac);
        this.name = name;
        this.surname = surname;
    }

    public String getID() {
        id = id+1;
        return IDpac = id.toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    @Override
    public String toString() {
        if (medication.idPac.equals(id.toString())){
            String medAtrib = medication.returnMedAtribut();
            return "Pacient{" +
                    "ID='" + IDpac + '\'' +
                    ", name='" + name + '\'' +
                    ", Surname='" + surname + '\'' +
                    "" + medAtrib + "\n"+'}';
        }
        return null;
    }
}
