package cz.cvut.srotypet.observer;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Person implements MyListSubscriber {
    private static final Logger log = LogManager.getLogger(Person.class);

    @Override
    public void notifyChanged(Object s) {
        log.info("Zaznamenana osoba - {} pridana do backpacku", () -> s);
    }
}
