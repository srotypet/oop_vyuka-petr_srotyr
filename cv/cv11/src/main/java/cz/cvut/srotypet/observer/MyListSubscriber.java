package cz.cvut.srotypet.observer;

public interface MyListSubscriber<T> {
    void notifyChanged(T s);
}
