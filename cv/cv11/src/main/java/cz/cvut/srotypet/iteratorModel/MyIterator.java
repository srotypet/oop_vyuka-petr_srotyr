package cz.cvut.srotypet.iteratorModel;

import java.util.Iterator;

public class MyIterator<T> implements Iterator {

    private int cursor = -1;
    private T[] data;

    public MyIterator(T[] data){
        this.data = data;
    }

    @Override
    public boolean hasNext() {
        return cursor + 1 < data.length;
    }

    @Override
    public T next() {
        if(!this.hasNext()){
            throw new IllegalStateException("No next item in data");
        }
        cursor++;
        return data[cursor];
    }
}
