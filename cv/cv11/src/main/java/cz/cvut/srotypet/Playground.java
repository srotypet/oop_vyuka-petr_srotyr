package cz.cvut.srotypet;

import cz.cvut.srotypet.iteratorModel.MyList;
import cz.cvut.srotypet.observer.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Playground {

    private static final Logger log = LoggerFactory.getLogger(Playground.class); //Odkud se loguje (Pozor at je tam trida ze ktery se loguje, jinak se to blbe hleda)

    public void play(){
        log.info("info");
        log.warn("warn");
        log.error("errora");
    }

    public void iterator(){
        List<String> strings = Arrays.asList("A", "B", "C");
        for(String s: strings){
            log.info(s);
        }
        Iterator<String> iterator = strings.iterator();
        /*for(iterator.hasNext()){
            log.info(iterator.next());
        }*/

        String[] arr = {"A","B","C"};
    }

    public void observer(){
        //MyList<String> str = "A";
        Person prson = new Person();
        MyList<String> backpack = new MyList<>();

        backpack.subscribe(prson);

        backpack.add("A sub");
        backpack.add("B sub");
        backpack.add("C sub");
    }
}
