package cz.cvut.srotypet.iteratorModel;

import cz.cvut.srotypet.observer.MyListSubscriber;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.List;

public class MyList<T> implements Iterable<T> {

    private List<T> data = new ArrayList<>();
    private List<MyListSubscriber<T>> subscribers = new ArrayList<>();

    public void add(T item){
        data.add(item);
        this.notifySubscribers(item);
    }

    public void notifySubscribers(T data){
        subscribers.forEach(myListSubscriber -> myListSubscriber.notifyChanged(data));
    }

    public void subscribe(MyListSubscriber<T> myListSubscriber){
        subscribers.add(myListSubscriber);
    }


    @Override
    public void forEach(Consumer<? super T> action) {

    }

    @Override
    public Spliterator spliterator() {
        //return new MyIterator<>();
        return null;
    }

    @Override
    public Iterator<T> iterator() { return new MyIterator<>(data); }  //Tady mi hazi kompiler chybu ale nemuzu prijit co mu vadi...
}
