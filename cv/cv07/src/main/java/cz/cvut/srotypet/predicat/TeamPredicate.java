package cz.cvut.srotypet.predicat;

import cz.cvut.srotypet.teamModel.Team;

public interface TeamPredicate {
    public boolean evaluate(Team team);
}
