package cz.cvut.srotypet.teamModel;

public class Team {
    private String teamName;
    //private Integer teamId;
    public Integer teamScore = 0;

    public Team(String teamName) {
        this.teamName = teamName;
        //this.teamId = teamId;
        this.teamScore = 0;
    }

    public String getTeamName() {
        return teamName;
    }

    public Integer getTeamScore() {
        return teamScore;
    }

    public void setTeamScore(Integer teamScore) {
        this.teamScore += teamScore;
    }

    @Override
    public String toString() {
        return "Tym " +
                teamName + '\'';
    }
}
