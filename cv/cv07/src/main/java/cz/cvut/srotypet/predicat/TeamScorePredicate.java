package cz.cvut.srotypet.predicat;

import cz.cvut.srotypet.teamModel.Team;

public class TeamScorePredicate implements TeamPredicate {

    @Override
    public boolean evaluate(Team team) {
        if (team.getTeamScore() > 3){
            return true;
        }
        else {
            return  false;
        }
    }
}
