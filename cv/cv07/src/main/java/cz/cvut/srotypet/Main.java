package cz.cvut.srotypet;

import cz.cvut.srotypet.predicat.GenericPredicate;
import cz.cvut.srotypet.predicat.TeamNamePredicate;
import cz.cvut.srotypet.predicat.TeamPredicate;
import cz.cvut.srotypet.teamModel.Box;
import cz.cvut.srotypet.teamModel.Team;

import java.util.ArrayList;
import java.util.List;

public class Main {
    /*public static void main(String[] args){
        codeFromClass();



        }*/

    public void genericPredicate(){
        List<Team> teams = new ArrayList<>();
        Team a = new Team("A");
        a.setTeamScore(4);
        Team b = new Team("B");
        b.setTeamScore(2);
        Team c = new Team("C");
        c.setTeamScore(3);
        Team d = new Team("D");
        d.setTeamScore(10);
        Team e = new Team("E");
        e.setTeamScore(0);
        teams.add(a);
        teams.add(b);
        teams.add(c);
        teams.add(d);
        teams.add(e);
        List<Team> teamWithMoreThan3TeamScore = predList(value -> value.getTeamScore() < 3, teams);
        for (Team team:teamWithMoreThan3TeamScore) {
            System.out.println(team.getTeamName()+ " s " + team.getTeamScore() + " body");
        }

    }

    public <T> List<T> predList(GenericPredicate<T> pred, List<T> list){
        list.removeIf(pred::evaluate);
        return list;
    }

    public void staticPredicates() {
        TeamNamePredicate teamNamePredicate = new TeamNamePredicate("A");
        if (teamNamePredicate.evaluate(new Team("B"))) {
            System.out.println("NOT EQ");
        }
        if (teamNamePredicate.evaluate(new Team("A"))) {
            System.out.println("EQ");
        }
        if (evaluatePredicate(team -> team.getTeamName().equals("A"), new Team("A"))) {
            System.out.println("LAMBDA PRED EQ");
        }
        if (evaluatePredicate(new TeamPredicate() {
            @Override
            public boolean evaluate(Team team) { return team.getTeamName().equals("A"); }
        }, new Team("A"))) {
                System.out.println("LAMBDA PRED EQ");
            }
        }

    public boolean evaluatePredicate(TeamPredicate teamPredicate, Team team){return teamPredicate.evaluate(team);}

    public void codeFromClass() {
        TeamNamePredicate teamNamePredicate = new TeamNamePredicate("A");
        if (teamNamePredicate.evaluate((new Team("B")))) {
            System.out.println("not eq");
        }
        if (teamNamePredicate.evaluate((new Team("A")))) {
            System.out.println("EQ");
        }
        Team team = new Team("Tera");
        Box<String> stringBox = new Box();
        stringBox.add("6541");
        stringBox.add("Jaro");
        stringBox.add("Oraj");
        System.out.println(stringBox.get());
        Box<Team> teamBox = new Box();
        teamBox.add(team);
        System.out.println(teamBox.get());
    }
}
