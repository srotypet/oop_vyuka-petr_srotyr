package cz.cvut.srotypet.predicat;

public interface GenericPredicate<T> {
    boolean evaluate(T value);
}
