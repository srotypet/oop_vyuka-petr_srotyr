package cz.cvut.srotypet.predicat;

import cz.cvut.srotypet.teamModel.Team;

public class TeamNamePredicate implements TeamPredicate {

    private String name;
    public TeamNamePredicate(String name){this.name = name;}

    @Override
    public boolean evaluate(Team team) {
        return team.getTeamName().equals(name); //kontroluje zda se jedna o tym name
    }
}
