package cz.cvut.srotypet;

public class MainOnTwo {
    public static void main(String[] args){
        Main main = new Main();
        //kod z hodiny
        System.out.println("Vystup kodu z hodiny:");
        main.codeFromClass();
        //kod z hodiny ve statickem predikatu
        System.out.println("\nVystup kodu z hodiny dodelany do statickeho predikatu:");
        main.staticPredicates();
        //genericky predikat + dodelani filtrace z predikatu
        System.out.println("\nVyustup dodelani filtrace na zaklade predikatu: ");
        main.genericPredicate();

    }
}
