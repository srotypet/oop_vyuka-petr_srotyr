package cz.cvut.srotypet.teamModel;

public class TeamBox<E extends Team> {
    private E value;

    public void add(E value){
        if(!value.getTeamName().equals("A")){
            this.value = value;
        }
    }
    public E get(){return value;}
}
