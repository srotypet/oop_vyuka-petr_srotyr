package cvut.cz.srotypet.CarList;

public class Car {
    private String sign;
    private String color;
    private String madeDate;
    private Motor motor;

    public Car(String sign, String color, String madeDate, Motor motor) {
        this.sign = sign;
        this.color = color;
        this.madeDate = madeDate;
        this.motor = motor;
    }
    /*public String returnAtribCar(){
        String atribMotor = motor.returnAtribMotor();
        return sign + " " + color + madeDate + atribMotor;
    }*/

    @Override
    public String toString() {
        String atribMotor = motor.returnAtribMotor();
        return  "Car{" + "\n" +
                "znascka='" + sign + "\n" +
                "barva='" + color + "\n" +
                "datum vyroby='" + madeDate + "\n" +
                "" + atribMotor + "\n\n" +
                '}';
    }
}
