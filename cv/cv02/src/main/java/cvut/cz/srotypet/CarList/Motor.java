package cvut.cz.srotypet.CarList;

public class Motor {
    private String fuel;
    private String capacity;

    public Motor(String fuel, String capacity) {
        this.fuel = fuel;
        this.capacity = capacity;
    }
    public String returnAtribMotor(){
        return "Palivo=" + fuel + "\n" + "objem=" + capacity;
    }
}
