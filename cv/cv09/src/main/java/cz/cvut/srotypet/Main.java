package cz.cvut.srotypet;

public class Main {
    public static void main(String[] args){
        //prostor pro singleton vzor...

        System.out.println("Builder Items");
        System.out.println("-----------------------------------------------------");
        Item item1 = new Item();
        Item item2 = new Item();
        Item item3 = new Item();

        item1.setID(1).setName("ostry klacek").setAbilities("Pich").setpower(5);
        item2.setID(2).setName("klacek").setpower(1);
        item3.setID(3).setName("klacek s papirem").setAbilities("Vytri...").setpower(10);

        System.out.println(item1);
        System.out.println(item2);
        System.out.println(item3);

        System.out.println("\n\nSingleton pripad");
        threadSafeSingleton();
    }

    public static void threadSafeSingleton(){
        new Thread("newThread"){
            @Override
            public void run(){ System.out.println(CommandLineUi.getInstanceBP()); }
        }.start();

        System.out.println(CommandLineUi.getInstanceBP());
        System.out.println(CommandLineUi.getInstanceBP());
    }
}
