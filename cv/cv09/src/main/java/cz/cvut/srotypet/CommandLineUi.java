package cz.cvut.srotypet;

public class CommandLineUi {
    private static CommandLineUi INSTANCE;

    private CommandLineUi(){

    }

    private static class CommandLineUiHolder{
        private static CommandLineUi COMMANDLINEUI = new CommandLineUi();
    }

    public static CommandLineUi getInstanceBP() { return CommandLineUiHolder.COMMANDLINEUI; }

    public synchronized static CommandLineUi getInstance(){
        if(INSTANCE == null){
            INSTANCE = new CommandLineUi();
        }
        return INSTANCE;
    }
}
