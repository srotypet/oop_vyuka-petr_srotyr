package cz.cvut.srotypet;

public class Item {
    private String name;
    private int id;
    private String abilities;
    private int power;

    public Item setID(int id){
        this.id = id;
        return this;
    }
    public Item setName(String name){
        this.name = name;
        return this;
    }

    public Item setAbilities(String abilities){
        this.abilities = abilities;
        return this;
    }

    public Item setpower(int power){
        this.power = power;
        return this;
    }

    @Override
    public String toString() {
        return "name= " + name +
                ", id= " + id +
                ", abilities= " + abilities +
                ", power= " + power;
    }
}
