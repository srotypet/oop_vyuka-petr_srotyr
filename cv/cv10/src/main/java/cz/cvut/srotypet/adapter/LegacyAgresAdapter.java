package cz.cvut.srotypet.adapter;

public class LegacyAgresAdapter implements AgresAdapter{
    private LegacyAgresService adresService;

    public LegacyAgresAdapter(LegacyAgresService adresService) {this.adresService = adresService;}

    @Override
    public String findAddress(String addressString) {
        return adresService.findAddress(addressString);
    }
}
