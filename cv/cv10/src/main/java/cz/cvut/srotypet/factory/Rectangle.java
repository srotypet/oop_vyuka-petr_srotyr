package cz.cvut.srotypet.factory;

public class Rectangle implements Polygon {
    int side = 4;

    @Override
    public Polygon createPolygon() {
        return new Rectangle();
    }

    public int getSide() {
        return side;
    }
}
