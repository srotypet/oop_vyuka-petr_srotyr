package cz.cvut.srotypet.flyweigth;

public class Car {
    private FlyweightCar flyweightCar;
    private String color;

    public Car(FlyweightCar flyweightCar, String color){
        this.flyweightCar = flyweightCar;
        this.color = color;
    }

    @Override
    public String toString() {
        return flyweightCar + " barva: " + color;
    }
}
