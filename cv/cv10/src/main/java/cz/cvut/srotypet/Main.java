package cz.cvut.srotypet;

import cz.cvut.srotypet.adapter.AgresAdapter;
import cz.cvut.srotypet.adapter.LegacyAgresAdapter;
import cz.cvut.srotypet.adapter.LegacyAgresService;
import cz.cvut.srotypet.adapter.NewAgresAdapter;
import cz.cvut.srotypet.decorator.Coopffee;
import cz.cvut.srotypet.decorator.CoopffeeWithMilk;
import cz.cvut.srotypet.decorator.CoopffeeWtihSugar;
import cz.cvut.srotypet.factory.Polygon;
import cz.cvut.srotypet.factory.Rectangle;
import cz.cvut.srotypet.factory.Triangle;
import cz.cvut.srotypet.flyweigth.Car;
import cz.cvut.srotypet.flyweigth.FlyweightCar;
import cz.cvut.srotypet.proxy.Counter;
import cz.cvut.srotypet.proxy.CounterProxy;
import cz.cvut.srotypet.proxy.ProxyCounter;

public class Main {
    public static void main(String[] args) {

        //Vyrobna tvaru na zakladu
        System.out.println("Vyrobna");
        Polygon polygonR = new Rectangle();
        Rectangle tangle = (Rectangle) polygonR.createPolygon();
        System.out.println("pocet stran prvniho tvaru: "+tangle.getSide());
        Polygon polygonT = new Triangle();
        Triangle angle = (Triangle) polygonT.createPolygon();
        System.out.println("pocet stran druheho tvaru: "+angle.getSide());

        //Dekorator - ozdobeni
        System.out.println("\nDekorator kafe");
        CoopffeeWithMilk cafe1 = new CoopffeeWithMilk();
        CoopffeeWtihSugar cafe2 = new CoopffeeWtihSugar();
        System.out.println(cafe1.reparoCafe());
        System.out.println(cafe2.reparoCafe());

        //Flyweight - mysi vaha
        System.out.println("\nFlyweight auta");
        FlyweightCar BMW = new FlyweightCar("BMW",250);
        FlyweightCar LADA = new FlyweightCar("LADA",150);
        FlyweightCar plechovka = new FlyweightCar("plechovka",80);
        Car car1 = new Car(BMW, "black");
        Car car2 = new Car(BMW, "darkgreen");
        Car car3 = new Car(LADA, "blue");
        Car car4 = new Car(plechovka, "rezava");
        Car car5 = new Car(plechovka, "prozrana");
        System.out.println(car1); System.out.println(car2); System.out.println(car3); System.out.println(car4); System.out.println(car5);

        //Adapter adres
        System.out.println("\nAdapter");
        AgresAdapter adres1 = new LegacyAgresAdapter(new LegacyAgresService());
        System.out.println(adres1.findAddress("sdasdo"));
        AgresAdapter adres2 = new NewAgresAdapter();
        System.out.println(adres2.findAddress("balbo"));

        //Proxy
        System.out.println("\nProxy");
        ProxyCounter counter = new CounterProxy(new Counter());
        System.out.print(counter.incrementAndGet());
        System.out.println(" Zalogovan");
        System.out.print(counter.incrementAndGet());
        System.out.println(" Zalogovan");
    }
}
