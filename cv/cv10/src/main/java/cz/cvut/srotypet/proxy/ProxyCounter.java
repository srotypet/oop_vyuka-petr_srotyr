package cz.cvut.srotypet.proxy;

public interface ProxyCounter {
    public int incrementAndGet();
}
