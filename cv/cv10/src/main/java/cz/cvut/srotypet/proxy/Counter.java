package cz.cvut.srotypet.proxy;

public class Counter implements ProxyCounter {
    private int counter;

    public int incrementAndGet(){return counter=counter+1;}
}
