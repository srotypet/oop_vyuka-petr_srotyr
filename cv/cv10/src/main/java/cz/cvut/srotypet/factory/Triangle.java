package cz.cvut.srotypet.factory;

public class Triangle implements Polygon {
    int side = 3;

    @Override
    public Polygon createPolygon() {
        return new Triangle();
    }

    public int getSide() {
        return side;
    }
}
