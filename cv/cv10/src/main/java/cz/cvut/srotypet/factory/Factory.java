package cz.cvut.srotypet.factory;

public class Factory {
    public Polygon createPolygon(Polygon polygon){
        if(polygon instanceof Rectangle){
            return  new Rectangle();
        }if(polygon instanceof Triangle) {
            return new Triangle();
        }return null;
    }
}
