package cz.cvut.srotypet.factory;

public interface Polygon {
    public Polygon createPolygon();
}
