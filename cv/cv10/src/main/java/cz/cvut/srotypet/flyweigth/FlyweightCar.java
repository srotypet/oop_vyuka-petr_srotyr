package cz.cvut.srotypet.flyweigth;

public class FlyweightCar {
    private String typ;
    private int speed;

    public FlyweightCar(String typ, int speed){
        this.typ = typ;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Znacka: " + typ + ", rychlost: " + speed;
    }
}
