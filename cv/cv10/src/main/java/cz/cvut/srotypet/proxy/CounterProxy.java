package cz.cvut.srotypet.proxy;

public class CounterProxy implements ProxyCounter {
    private Counter counter;

    public CounterProxy(Counter counter) { this.counter = counter; };

    @Override
    public int incrementAndGet() {
        int i = counter.incrementAndGet();
        return i;
    }
}
