package cz.cvut.srotypet;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Date;

public class Match {
private Team t1 = new Team("",0);
private Team t2 = new Team("",0);
private ArrayList<Event> events = new ArrayList<>();
public boolean startMatch = false;

    public Match(Team t1, Team t2){
        this.t1 = t1;
        this.t2 = t2;
    }

    public String getT1() {
        return t1.getTeamName();
    } //vraci jmeno 1 tymu v zapasu

    public String getT2() {
        return t2.getTeamName();
    } //vraci jmeno 2 tymu v zapasu

    public void startMatch(){
        startMatch = true; events.add(new Event(new Date(), 0, Status.START, IdTeam.JUDGER));}  //Sudi zasahuje zapas
    public void addEvent(int time, Status status, IdTeam id) {
        if (startMatch == true) {
            if (status == Status.BALL) {events.add(new BallEvent(new Date(), time, status, id));}
            if (status == Status.NET)  {events.add(new NetEvent(new Date(), time, status, id));}
            if (status == Status.UNDERNET) {events.add(new UnderNetEvent(new Date(), time, status, id));}
            if (status == Status.OUT) {events.add(new OutEvent(new Date(), time, status, id));}}
            //events.add(new Event(new Date(), time, status, id));} //prida nahodnou udalost nahodnemu tymu v zapase
            else {
                System.out.println("Sampionat nezacal, nelze dat mic");
            }
        }

    public void endMatch(int time){
        startMatch = false; events.add(new Event(new Date(),time, Status.STOP, IdTeam.JUDGER));} //Sudi ukoncuje zapas

    //Vypocita score zapasu podle toho kolik tymy dali micu v zapase
    public int[] calculateScore(){
        int t1=0, t2=0;
        for(Event event:events){
            if (event.idTeam == IdTeam.T1 && event.status == Status.BALL){
                t1 += 1;
            }if (event.idTeam == IdTeam.T2 && event.status == Status.BALL){
                t2 +=1;
            }
        }return new int[] {t1,t2};
    }

    //Vypocita body tymu podle vyhranych zapasu (pokud byla uhrana remize nedostava body zadny tym
    public void calculatePoints(){
        int tp1 = 0, tp2 = 0;
        //int c1 = calculateScore()[0];
        //int c2 = calculateScore()[1];
            if(calculateScore()[0]>calculateScore()[1]){ tp1 = 1; t1.setTeamScore(tp1);}
            if(calculateScore()[1]>calculateScore()[0]){ tp2 = 1; t2.setTeamScore(tp2);}
            if(calculateScore()[0]==calculateScore()[1]){ tp1 = 0; t1.setTeamScore(tp1);}
            else{ tp2 = 0; t2.setTeamScore(tp2);}
            //else{System.out.println("\nneporovnatelne vysledky");}
    }

    public void printLog(){
        System.out.println("\nZaznam zapasu " + t1.getTeamName() + " (T1)" + " proti " +
                t2.getTeamName() + " (T2)" + " \n"+ "-------------------------------------------------------------------------");
        for (Event event:events){
            System.out.print(event.printMatch());
        }
    }

    /*@Override
    public String toString() {
        return "Zaznam zapasu " + t1.getTeamName() + " proti " +
                         t2.getTeamName() + " \n"+ "-------------------------------------------------------------------------\n" + events + " \n"
                +"-------------------------------------------------------------------------\n" + "\n";
    }*/
}
