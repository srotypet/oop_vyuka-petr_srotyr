package cz.cvut.srotypet;

import java.util.List;

public interface MatchGenerator {
    List<Match> generateMatches(List<Team> team);
}
