package cz.cvut.srotypet;

import java.util.HashSet;

public class Playground {

    public void play() {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("Andre");
        hashSet.add("Zumba");
        hashSet.add("Mongo");
        hashSet.add("Cecil");
        hashSet.add("Boda");
        hashSet.add("Koala");
        hashSet.stream().sorted();

        System.out.println(hashSet + "\n\n");

        Team t1 = new Team("Ocasomuti",1);
        Team t2 = new Team("Zizalomuti",2);
        Team t3 = new Team("Konomuti",3);
        Team t4 = new Team("Bezsamponi",4);

        //registrace tymu do zapasu
        Championship championship = new Championship(new TableMatchGenerator());
        championship.register(t1);
        championship.register(t2);
        championship.register(t3);
        championship.register(t4);
        //System.out.println(championship);

        //Generovani zapasu
        championship.generateMatches();
        for (Match match: championship.getMatches()) {
            championship.simulatChamp(match);
        }
        //volani vypisovacich metod do konzole
        championship.printMatch();
        championship.printScore();
        championship.printPoints();
        championship.printWiener();
    }
}
