package cz.cvut.srotypet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Championship {
    private MatchGenerator matchGenerator;
    private List<Team> teams = new ArrayList<>();
    private List<Match> matches = new ArrayList<>();
    public void register(Team team) {teams.add(team);}
    public List<Match> getMatches() {
        return matches;
    }
    public Championship(MatchGenerator matchGenerator) {this.matchGenerator = matchGenerator; }


    public void generateMatches(){
        System.out.println("registrovane tymy:");
        int count = 1;
        for (Team team: teams) {
            System.out.println(count + ". " + team.getTeamName());
            count++;
        }
        System.out.println("\n");

        matches = matchGenerator.generateMatches(teams);

        //teams.removeAll(Collections.singleton(null));

        /*
        //Vygenerovani zapasu kazdy s kazdym
        if (teams.size() <= 1){
            System.out.println("Nedostatek tymu v turnaji!");
            throw new IllegalStateException("Nedostatek tymu v turnaji!");
        }
        //System.out.println(teams);
        for (int i = 0; i < teams.size(); i++){
            for (int j=i+1;j<teams.size(); j++){
                matches.add(new Match(teams.get(i), teams.get(j)));
            }
        }*/

        //generovani zapasu kazdy s kazdym i s odvetnymi zapasy (nedoimplementovano)
        /*for(int i=1; i<=4;i++){
            for(int j=1; j<=4;j++){
                if(i!=j){
                    System.out.println("souboj mezi "+i+" a "+j);
                    Random rnd = new Random();
                    int t1Score=0; int t2score=0;
                    for(int k=0; k<=10; k++){
                        int goalChance = rnd.nextInt(50);
                        if (goalChance >25){
                            t1Score +=1;
                        }
                    }
                    for(int l=0; l<=10; l++){
                        int goalChance = rnd.nextInt(50);
                        if (goalChance >25){
                            t2score +=1;
                        }
                    }
                    System.out.println("Score: "+ "Team1: "+ t1Score + " Team2: "+ t2score);
                }
            }
        }*/
    }
    public void simulatChamp(Match match){
        /*Simulace zapasu. Pocet kol v zapasu je 20 + nahodny pocet (0-10).
          Kazde kolo muze tym dat bod nebo nahodnou udalost ktera neni hodnocena.
          Kazdy ziskany bod se pricita danemu tymu do skore v metode printScore
         */

        //System.out.println(matches);
        //int counter1=0;
        //int counter2=0;
        match.startMatch();
        Random rnd1 = new Random();
        Random rnd2 = new Random();

        for(int i = 1; i<rnd1.nextInt(10)+20; i++){
            int kartnahody = rnd2.nextInt(7)+1;
            if(kartnahody == 1){
                match.addEvent(rnd1.nextInt(78)+1, Status.BALL, IdTeam.T1); //counter1++; //counteri meli puvodne pocitat balls, vyreseno jinak
                 }
            if(kartnahody == 2){
                match.addEvent(rnd1.nextInt(78)+1, Status.BALL, IdTeam.T2); //counter2++;
                }
            if(kartnahody == 3){
                match.addEvent(rnd1.nextInt(78)+1, Status.OUT, IdTeam.T1);}
            if(kartnahody == 4){
                match.addEvent(rnd1.nextInt(78)+1, Status.OUT, IdTeam.T2);}
            if(kartnahody == 5){
                match.addEvent(rnd1.nextInt(78)+1, Status.UNDERNET, IdTeam.T1);}
            if(kartnahody == 6){
                match.addEvent(rnd1.nextInt(78)+1, Status.UNDERNET, IdTeam.T2);}
            if(kartnahody == 7){
                match.addEvent(rnd1.nextInt(78)+1, Status.NET, IdTeam.T1);}
            if(kartnahody == 8){
                match.addEvent(rnd1.nextInt(78)+1, Status.NET, IdTeam.T2);}
        }
        match.endMatch(80);
    }

    //Vytisknuti zapasu s udalostmi
    public void printMatch(){
        for (Match match: matches) {
            match.printLog();
        }
    }

    // Vytisknuti ziskanych bodu mezi tymy v zapasu
    public void printScore(){
        System.out.println("\n\nVysledky zapasu");
        System.out.println("============================");
        for (Match match: matches) {
            System.out.println(match.getT1() + "\t" +  match.calculateScore()[0] + ":" + match.calculateScore()[1] + "\t" + match.getT2());
        }
        System.out.println("============================\n");
    }

    /*public int[] calculatePoints(){
        int tp1=0, tp2=0;
        for (Match match: matches) {
            if(match.calculateScore()[0]>match.calculateScore()[1]){ tp1 += 1; }
            if(match.calculateScore()[1]>match.calculateScore()[0]){ tp2 += 1; }
        }return new int[] {tp1,tp2};
    }*/

    //Vytisknuti ziskanych bodu daneho tymu (pokud tym vyhraje +1bod kdyz prohraje +0bod)
    public void printPoints(){
        for (Match match: matches) {
            match.calculatePoints();
        }
        System.out.println("\nBody tymu");
        System.out.println("===============");
        for (Team team: teams) {
            System.out.println(team.getTeamName() + "\t" +  team.getTeamScore());
        }
        System.out.println("===============");
    }

    //zjisteni a vytisknuti viteze champu (Vyhrava tym s nejvyssim poctem bodu
    //pokud je skore nerozhodne je proveden roztrel ktery urci vitezny tym
    public void printWiener(){
        Random rnd = new Random();
        Team wiener = new Team("None", 5);
        int max=0;
        for (Team team:teams) {
            if(team.getTeamScore() > max){
                max = team.getTeamScore();
                wiener = team;
            }if(team.getTeamScore() == max) {
                //System.out.println("Skore nerozhodne byl proveden roztrel");
                if (rnd.nextInt(50) < 25) {
                    wiener = team;
                }
            }
        }
        if (wiener.getTeamName() != "None"){
        System.out.println("\n=======================================================");
        System.out.print("Vitezem se pro tento sampon stava: ");
        System.out.println(wiener);
        System.out.println("=======================================================");}
        else{
            System.out.println("Vitez neexistuje (jsou tymy registrovane a probehli zapasy?)");
        }
    }

    @Override
    public String toString() {
        return "Championship{" +
                "teams=" + teams +
                '}';
    }
}
