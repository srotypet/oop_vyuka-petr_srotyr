package cz.cvut.srotypet;

public enum Status {
    BALL,
    START,
    STOP,
    NET,
    UNDERNET,
    OUT
}
