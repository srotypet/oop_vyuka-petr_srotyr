package cz.cvut.srotypet;

import org.junit.Assert;
import org.junit.Test;

import java.security.AccessControlException;

public class MatchTest {

    @Test
    public void AddGoalBeforeStart(){
        Team t1 = new Team("t1", 1);
        Team t2 = new Team("t2", 2);
        Match match = new Match(t1,t2);
        try{
            match.addEvent(50,Status.BALL, IdTeam.T1);
        }catch (AccessControlException e){
        Assert.assertFalse(match.startMatch);}
    }
}
