package cz.cvut.srotypet;

import org.junit.Test;

public class MainTest {

    @Test
    public void simulateChamp_printWienerChamp(){
        Team t1 = new Team("Kora",1);
        Team t2 = new Team("Kera",2);
        Team t3 = new Team("Kura",3);
        Team t4 = new Team("Kira",4);

        Championship championship = new Championship(new TableMatchGenerator());
        championship.register(t1);
        championship.register(t2);
        championship.register(t3);
        championship.register(t4);

        championship.generateMatches();
        for (Match match: championship.getMatches()) {
            championship.simulatChamp(match);
        }
        try{
        championship.printMatch();
        championship.printScore();
        championship.printPoints();
        championship.printWiener();
        }catch(IllegalStateException e){
            System.out.println(e);
        }
    }
}
