package cz.cvut.srotypet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.lang.model.type.NullType;

public class ChampionshipTest {

    /*@Mock
    private MatchGenerator(){

    }*/

    @Before
    public void setup(){
        Mockito.reset();
    }

    @Test(expected = IllegalStateException.class)
    public void generateMatches_lessThanOneTeamRegister_throwException(){
        Team t1 = new Team("Teemo", 1);

        Championship championship = new Championship(new TableMatchGenerator());
        championship.register(t1);

        championship.generateMatches();
    }
    @Test
    public void generateMatches_validTeamNumber_generateRightNUmberMatch(){
        Team t1 = new Team("Kora",1);
        Team t2 = new Team("Kera",2);

        Championship championship = new Championship(new TableMatchGenerator());
        championship.register(t1);
        championship.register(t2);
        championship.generateMatches();
        for (Match match: championship.getMatches()) {
            championship.simulatChamp(match);
        }

        Assert.assertEquals(1,championship.getMatches().size());
    }

    @Test
    public void wiener_wienerIsNull(){
        Team t1 = new Team("Kora",1);
        Team t2 = new Team("Kera",2);
        Team t3 = new Team("Kura",3);
        Team t4 = new Team("Kira",4);

        Championship championship = new Championship(new TableMatchGenerator());
        championship.register(t1);
        championship.register(t2);
        championship.register(t3);
        championship.register(t4);

        championship.printWiener();
    }
}
