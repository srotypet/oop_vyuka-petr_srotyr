package cz.cvut.srotypet;

public class Playground {
    public void play() {
    int i = 0;
    i = passByValue(i);
    System.out.println(i);
    mutability();
    comparality();
    }

    public void comparality(){
        Integer a1 = 8000;
        Integer a2 = 8000;
        Integer a3 = 200;
        Integer a4 = 200;
        Integer a5 = 100;
        Integer a6 = 100;
        Integer a7 = 50;
        Integer a8 = 50;
        int b1 = 8000;
        int b2 = 8000;
        boolean a8k = a1==a2;
        boolean a200 = a3==a4;
        boolean a100 = a5==a6;
        boolean a50 = a7==a8;
        System.out.println("\nInteger");
        System.out.println("8k==8k   -> "+a8k);
        System.out.println("200==200 -> "+a200);
        System.out.println("100==100 -> "+a100);
        System.out.println("50==50   -> "+a50);
        boolean b = b1==b2;
        System.out.println("int");
        System.out.println("8k = 8k  -> "+b);
        System.out.println("======================");

        String s1 = "nazdar";
        String s2 = "nazdar";
        System.out.println(s1==s2);
        System.out.println("=========================\n");
        System.out.println("======Ukol s pacosi======\n");
        System.out.println("=========================\n");

        Pacient pacienti[] = new Pacient[5];
        pacienti[0] = new Pacient("Peroslav","Prtacnik", new Medication("Nespavost", "Di spat"));
        //pacienti[1] = new Pacient("Carda","Retarda", new Medication("Sportak", "Najdi chlapa"));
        //pacienti[2] = new Pacient("Chorah","Chuchel", new Medication("Nenazranost", "Nezer"));
        System.out.println(pacienti[0]);
        System.out.println(pacienti[1]);
        //System.out.println(pacienti[2]);

        /*pac.setName("Peroslav");
        pac.setSurname("Sprtacnik");
        String idpac = pac.getID();
        med.setIDpac(idpac);*/

        //System.out.println(med);
        //System.out.println(id);

    }

    public void mutability(){
        String name = "Ptacnik" + "Ostrostrelec";
        name.replace("t","d");
        System.out.println(name);
        String[] arr = {"W","A","S","D"};

        String result="";
        StringBuilder sb = new StringBuilder();
        for (String s:arr){
            sb.append(s);
        }
        System.out.println(sb.toString());
    }

    public int passByValue( int a){
        a++;
        return a;
    }
}
