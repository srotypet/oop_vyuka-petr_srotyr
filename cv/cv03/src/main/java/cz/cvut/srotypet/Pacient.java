package cz.cvut.srotypet;

public class Pacient {
    public String IDpac;
    private String name;
    private String surname;
    private Integer id = 0;
    private Medication medication;

    public Pacient(String name, String surname, Medication medication){
        String idpac = getID();
        medication.medIdPac(idpac);
        this.name = name;
        this.surname = surname;
        this.medication = medication;
        //Medication med = new Medication("Nespavost","di spat", id.toString());
        //med.medIdPac(IDpac);
    }

    public String getID() {
        id = id+1;
        return IDpac = id.toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    @Override
    public String toString() {
        if (medication.idPac.equals(id.toString())){
            String medAtrib = medication.returnMedAtribut();
            return "Pacient{" +
                "ID='" + IDpac + '\'' +
                ", name='" + name + '\'' +
                ", Surname='" + surname + '\'' +
                "" + medAtrib + "\n"+'}';
        }
        return null;
    }
}
