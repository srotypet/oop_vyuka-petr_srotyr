package cz.cvut.srotypet;

import java.util.Random;

public class Medication {
    private int idMed;
    public String idPac;
    private String disease;
    private String curse;
    private int id=100;

    public void medIdPac(String idPac){
        this.idPac = idPac;
    }
    public Medication(String disease, String curse){
        getID();
        //this.idPac = idPac;
        this.disease = disease;
        this.curse = curse;
    }

    public int getID() {
        //Random rnd = new Random();
        //int add = rnd.nextInt(1000);
        //id = id + add;
        id++;
        return idMed = id;
    }

    public void setIDpac(String IDpac) {
        this.idPac = IDpac;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public void setCursed(String cursed) {
        this.curse = cursed;
    }

    //@Override
    public String returnMedAtribut() {
        return " -> Medication{" +
                "IDmed=" + idMed +
                ", IDpac='" + idPac + '\'' +
                ", Disease='" + disease + '\'' +
                ", Curse='" + curse + '\'' +
                '}';
    }
    /*public enum disease{
        ponorkova,
        nespavost,
        litavka
    }*/
}
