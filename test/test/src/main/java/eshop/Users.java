package eshop;

import java.util.Observer;

public class Users implements Observer {
    private String userName;

    public void Users(String userName){
        this.userName = userName;
    }

    @Override
    public void update(java.util.Observable o, Object arg) {
        System.out.println("Vyprodane zbozi bylo doplneno, zprava pro odberatele: " + userName);
    }

    public void notifyUsers(){
        System.out.println("zbozi naskladneno");
    }
}
