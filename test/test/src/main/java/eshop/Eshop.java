package eshop;

import java.util.ArrayList;
import java.util.Observable;

public class Eshop extends Observable {
    private ArrayList<Items> items = new ArrayList<>();

    public void notification(){
        if(items.get(0).getAmount()>0){
            setChanged();
            notifyObservers();
        }
    }

    public void madeItem(){
        Items item1 = new Items("rohlik", 0);
        Items item2 = new Items("houska", 0);
        Items item3 = new Items("kolac", 0);
        Items item4 = new Items("chleba", 0);
        Items item5 = new Items("rohlik", 0);
        items.add(item1);items.add(item2);items.add(item3);items.add(item4);items.add(item5);
    }

    public void loadStockpile(){
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getAmount()==0){
                //System.out.println("zbozi bylo naskladneno");
                items.get(i).setAmount(10);
            }
        }
    }
}
