package eshop;

public class Items {
    private String name;
    private int amount;

    public Items(String name, int amount){
        this.name = name;
        this.amount = amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
}
