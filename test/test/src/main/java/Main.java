import eshop.Eshop;
import eshop.Users;

import java.util.Observer;

public class Main {
    public static void main(String[] args){
        Users user1 = new Users();
        user1.Users("Karel");
        Users user2 = new Users();
        user2.Users("Kuba");
        Users user3 = new Users();
        user3.Users("Pekar");
        Users user4 = new Users();
        user4.Users("Zvonar");
        Users user5 = new Users();
        user5.Users("Zelinar");
        Eshop shop = new Eshop();

        shop.addObserver((Observer) user1);
        shop.addObserver((Observer) user2);
        shop.addObserver((Observer) user3);
        shop.addObserver((Observer) user4);
        shop.addObserver((Observer) user5);
        shop.madeItem();
        shop.loadStockpile();
        shop.notification();
    }
}
