package cz.cvut.srotypet;

import cz.cvut.srotypet.model.Patient;
import cz.cvut.srotypet.service.PatientServiceImpl;
import cz.cvut.srotypet.service.legacy.LegacyAnamnesisServiceImpl;
import cz.cvut.srotypet.service.legacy.LegacyPatientServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class MedicalAppTest {

    @Test
    public void getPatient_whenCorrectPersonalIdSupplied_patientReturned(){
        LegacyAnamnesisServiceImpl anamnesisSpy = new LegacyAnamnesisServiceImpl();
        LegacyPatientServiceImpl patientSpy = new LegacyPatientServiceImpl();

        PatientServiceImpl patientService = new PatientServiceImpl();
        patientService.setLegacyAnamnesisService(anamnesisSpy);
        patientService.setLegacyPatientService(patientSpy);

        Patient patient = patientService.getPatient("456");
        Assert.assertNotNull(patient);
        System.out.println(patient);
    }

    @Test
    public void getPatient_whenCalledSecondTime_legacyServicesNotCalled(){
        //Spy je hybrid mezi Mock a reálnou implementací, umožní nám tedy využívat konkrétní implementaci a zároveň
        //ji v případě potřeby i mockovat, pokud mock nemá scénář, vrací null, pokud spy nemá scénář (when), provádí realnou
        //implementaci -> je postaveno na stejném vzoru, který zde máte implementovat
        LegacyAnamnesisServiceImpl anamnesisSpy = Mockito.spy(new LegacyAnamnesisServiceImpl());
        LegacyPatientServiceImpl patientSpy = Mockito.spy(new LegacyPatientServiceImpl());

        PatientServiceImpl patientService = new PatientServiceImpl();
        patientService.setLegacyAnamnesisService(anamnesisSpy);
        patientService.setLegacyPatientService(patientSpy);

        Patient patient = patientService.getPatient("123");
        Patient patient2 = patientService.getPatient("123");

        Assert.assertEquals(patient, patient2);

        Mockito.verify(anamnesisSpy, Mockito.times(1)).getData(Mockito.eq("123"));
        Mockito.verify(patientSpy, Mockito.times(1)).getData(Mockito.eq("123"));

    }
}
