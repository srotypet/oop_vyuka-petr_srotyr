package cz.cvut.srotypet.service.legacy;

import cz.cvut.srotypet.model.Anamnesis;

import java.util.HashMap;
import java.util.Map;

public class LegacyAnamnesisServiceImpl implements LegacyAnamnesisService {

    private static final Map<String, Anamnesis> ANAMNESIS = new HashMap<>();

    public LegacyAnamnesisServiceImpl() {
        this.initAnamnesis();
    }

    @Override
    public Anamnesis getData(String personalId) {
        try {
            //sleep for 3s, simulate long running legacy service
            Thread.sleep(3000L);
            return ANAMNESIS.get(personalId);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public void initAnamnesis() {
        Anamnesis anamnesis = new Anamnesis();
        anamnesis.setDescription("AAAAAAA");

        ANAMNESIS.put("123", anamnesis);

        anamnesis = new Anamnesis();
        anamnesis.setDescription("BBBBBBB");

        ANAMNESIS.put("456", anamnesis);

        anamnesis = new Anamnesis();
        anamnesis.setDescription("CCCCCCC");

        ANAMNESIS.put("789", anamnesis);
    }
}
