package cz.cvut.srotypet.service.legacy;

import cz.cvut.srotypet.model.Anamnesis;

public interface LegacyAnamnesisService {
    Anamnesis getData(String personalId);
}
