package cz.cvut.srotypet.service.legacy;

import cz.cvut.srotypet.model.Patient;

public interface LegacyPatientService {
    Patient getData(String personalId);
}
