package cz.cvut.srotypet.service.legacy;

import cz.cvut.srotypet.model.Patient;

import java.util.HashMap;
import java.util.Map;

public class LegacyPatientServiceImpl implements LegacyPatientService {

    private static final Map<String, Patient> PATIENTS = new HashMap<>();

    public LegacyPatientServiceImpl(){
        this.initPatients();
    }

    @Override
    public Patient getData(String personalId) {
        try {
            //sleep for 3s, simulate long running legacy service
            Thread.sleep(3000L);
            return PATIENTS.get(personalId);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public void initPatients(){
        Patient patient = new Patient();
        patient.setPersonalId("123");
        patient.setForename("Adam");
        patient.setSurname("Vojtěch");

        PATIENTS.put(patient.getPersonalId(), patient);

        patient = new Patient();
        patient.setPersonalId("456");
        patient.setForename("Andrej");
        patient.setSurname("Babiš");

        PATIENTS.put(patient.getPersonalId(), patient);

        patient = new Patient();
        patient.setPersonalId("789");
        patient.setForename("Alena");
        patient.setSurname("Schillerová");

        PATIENTS.put(patient.getPersonalId(), patient);
    }
}
