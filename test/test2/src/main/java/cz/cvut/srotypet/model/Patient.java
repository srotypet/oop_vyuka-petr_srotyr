package cz.cvut.srotypet.model;

public class Patient {
    private String personalId;
    private String forename;
    private String surname;

    private Anamnesis anamnesis;

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Anamnesis getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(Anamnesis anamnesis) {
        this.anamnesis = anamnesis;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "personalId='" + personalId + '\'' +
                ", forename='" + forename + '\'' +
                ", surname='" + surname + '\'' +
                ", anamnesis=" + anamnesis +
                '}';
    }
}
