package cz.cvut.srotypet.service;

import cz.cvut.srotypet.model.Patient;

public interface PatientService {

    Patient getPatient(String personalId);
}
