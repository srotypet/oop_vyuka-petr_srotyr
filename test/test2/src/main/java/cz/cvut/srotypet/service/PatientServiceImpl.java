package cz.cvut.srotypet.service;

import cz.cvut.srotypet.model.Anamnesis;
import cz.cvut.srotypet.model.Patient;
import cz.cvut.srotypet.service.legacy.LegacyAnamnesisService;
import cz.cvut.srotypet.service.legacy.LegacyPatientService;

public class PatientServiceImpl implements PatientService {
    private LegacyPatientService legacyPatientService;
    private LegacyAnamnesisService legacyAnamnesisService;

    @Override
    public Patient getPatient(String personalId) {
        Patient patient = legacyPatientService.getData(personalId);
        Anamnesis anamnesis = legacyAnamnesisService.getData(personalId);
        patient.setAnamnesis(anamnesis);
        return patient;
    }

    public void setLegacyAnamnesisService(LegacyAnamnesisService legacyAnamnesisService) {
        //TODO nápověda - aby integrační test procházel, je nutné zde poupravit, v opačném případě je nutné
        // upravit inicializaci legacy service přímo v testu
        this.legacyAnamnesisService = legacyAnamnesisService;
    }

    public void setLegacyPatientService(LegacyPatientService legacyPatientService) {
        //TODO nápověda - aby integrační test procházel, je nutné zde poupravit, v opačném případě je nutné
        // upravit inicializaci legacy service přímo v testu
        this.legacyPatientService = legacyPatientService;
    }

}
