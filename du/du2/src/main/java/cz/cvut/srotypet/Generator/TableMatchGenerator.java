package cz.cvut.srotypet.generator;

import cz.cvut.srotypet.championship.Match;
import cz.cvut.srotypet.championship.MatchStatus;
import cz.cvut.srotypet.championship.Team;

import java.util.ArrayList;
import java.util.List;

public class TableMatchGenerator implements MatchGenerator {

    @Override
    public List<Match> generateMatches(List<Team> teams){
        List<Match> matches = new ArrayList<>();
        if (teams.size() <= 1){
            System.out.println("Nedostatek tymu v turnaji!");
            throw new IllegalStateException("Nedostatek tymu v turnaji!");
        }
        //System.out.println(teams);
        for (int i = 0; i < teams.size(); i++){
            for (int j=i+1;j<teams.size(); j++){
                matches.add(new Match(teams.get(i), teams.get(j), MatchStatus.NEODEHRANO));
            }
        }return matches;
    }
}
