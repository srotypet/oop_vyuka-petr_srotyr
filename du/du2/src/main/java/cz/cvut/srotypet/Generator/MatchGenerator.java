package cz.cvut.srotypet.generator;

import cz.cvut.srotypet.championship.Match;
import cz.cvut.srotypet.championship.Team;
import java.util.List;

public interface MatchGenerator {
    List<Match> generateMatches(List<Team> team);
}
