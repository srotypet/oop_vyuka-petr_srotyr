package cz.cvut.srotypet.event;

import java.util.Date;

public class NetEvent extends Event {

    public NetEvent(Date date, int time, Status status, IdTeam idTeam){
        super(date, time, status, idTeam);
    }

    @Override
    public String printMatch() {
        return "Tym " + idTeam + " trefil sit\n";
    }
}
