package cz.cvut.srotypet.controlsUi;

import cz.cvut.srotypet.championship.Championship;
import cz.cvut.srotypet.championship.Match;
import cz.cvut.srotypet.championship.MatchStatus;
import cz.cvut.srotypet.generator.TableMatchGenerator;

import java.util.List;
import java.util.Scanner;

public class MatchesControlUi {
    //private Scanner sc = new Scanner(System.in, "Windows-1250");

    public void matchesControl(Championship champ, Scanner sc){
        List<Match> matches = champ.getMatches();
        boolean check = true;
        while(check) {
            System.out.println("------------------------------------");
            System.out.println("\nZvol zpusob vygenerovani zapasu:");
            System.out.println("a - kazdy s kazdym (table match generator)");
            System.out.println("b - kazdy s kazdym + odvetne zapasy");
            System.out.println("c - ...");
            String volbaGenu = sc.nextLine();
            if (volbaGenu.equals("a")) {
                champ.generateMatches(new TableMatchGenerator());
                check = false;
            }/*if(volbaGenu.equals("b")){
                System.out.println("Zvolena nedoimplementovana varianta");
            }*/
            else {
                System.out.println("zvolena neplatna varianta");
            }
        }
        chooseMatches(champ.getMatches(), champ, sc);
        //champ.printOnlyMatch();
    }

    public void chooseMatches(List<Match> matches, Championship champ, Scanner sc){
        Integer volba = 0;
        Integer volba2 = 0;
        boolean checker = true;
        while(checker) {
            System.out.println("\n---------------------------------------");
            System.out.println("Vyber zapas ktery ma byt odehran:");
            champ.printOnlyMatch();
            System.out.println("999. Ukoncit Sampionat a vyhodnotit viteze\n");
            volba = sc.nextInt();
            if(volba < matches.size()) {
                for (int i = 0; i <= matches.size(); i++) {
                    if (volba == i && matches.get(i).getMatchStatus() == MatchStatus.NEODEHRANO) {
                        System.out.println("Jakym zpusobem ma byt zapas odehran?");
                        System.out.println("1. Automaticky");
                        System.out.println("2. Manualne");
                        volba2 = sc.nextInt();
                        if (volba2 == 1) {
                            champ.simulatChamp(matches.get(i));
                            champ.printMatch();
                            champ.printScore();
                            champ.printWienerMatch(matches.get(i));
                            //champ.printPoints();
                            matches.get(i).setMatchStatus(MatchStatus.ODEHRANO);
                        }
                        if (volba2 == 2) {
                            champ.manualSimualChamp(matches.get(i), sc);
                            champ.printMatch();
                            champ.printScore();
                            champ.printWienerMatch(matches.get(i));
                            //champ.printPoints();
                            matches.get(i).setMatchStatus(MatchStatus.ODEHRANO);
                        } else {
                            System.out.println("Zadana neplatna hodnota");
                        }
                    }
                    if (volba == i && matches.get(i).getMatchStatus() == MatchStatus.ODEHRANO) {
                        System.out.println("zapas je jiz odehran!\n");
                    }//else{ System.out.println("Vyber pouze hodnoty uvedene! (0, 1, 2... 999)"); }
                }
            }
            //if(volba >= matches.size()){ System.out.println("Zvolen neexistujici zapas!"); }
            if(volba == 999){
                int counter = 0;
                for(int i =0; i< matches.size(); i++){
                    if(matches.get(i).getMatchStatus()==MatchStatus.ODEHRANO){
                        counter += 1;
                    }
                }
                if(counter == matches.size()){
                    champ.printScore();
                    champ.printPoints();
                    champ.printWiener();
                    checker = false;
                }else{
                    System.out.println("Nejsou odehrany vsechny zapasy!\n");
                }
            }
        }
    }
}
