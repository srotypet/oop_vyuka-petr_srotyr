package cz.cvut.srotypet.event;

import java.util.Date;

public class BallEvent extends Event {

    private int time;
    public BallEvent(Date date, int time, Status status, IdTeam idTeam){
        super(date, time, status, idTeam);
        this.time = time;
    }

    @Override
    public String printMatch() {
        return "Tym " + idTeam + " dal mic v case: " + time + "\n";
    }
}
