package cz.cvut.srotypet.controlsUi;

import cz.cvut.srotypet.championship.Championship;
import cz.cvut.srotypet.championship.Team;

import java.util.Scanner;

public class MainMenuControlUi {
    private Scanner sc = new Scanner(System.in, "UTF-8");
    private Championship champ = new Championship();
    private MatchesControlUi matchesControlUi = new MatchesControlUi();
    private boolean check = false;

    public void mainMenu(){
        int i = 0;
        int numberOfTeams = 4;
        //Championship championship = new Championship();
        System.out.println("Vytej v sampionatu!\n");
        System.out.println("==============================\n");
        //System.out.print("Zadej pocet tymu vstupujicich do turnaje: ");
        boolean checkCorrect = false;
        //start:
        while(!checkCorrect){
            Scanner scsuk = new Scanner(System.in, "UTF-8");
            System.out.print("Zadej pocet tymu vstupujicich do turnaje: ");
            try{
                numberOfTeams = scsuk.nextInt();
                if(numberOfTeams > 1) {
                    checkCorrect = true;
                }else{
                    System.out.println("Minimalni pocet tymu je 2!");
                }
            }catch(Exception e) {
                System.out.println("Zadana neplatna hodnota! pouze cela cisla"); //continue start;}
                //checkCorrect = false;
            }
        }
        Team[] team = new Team[numberOfTeams];
        System.out.println("--------------------------------");
        System.out.println("Hlavni menu\n");
        String volba = "0";
        while(!(volba.equals("4"))){
            System.out.println("1 - vytvorit a registrovat team");
            System.out.println("2 - zahajit sampionat");
            System.out.println("3 - vypis registrovanych teamu");
            System.out.println("4 - konec");
            volba = sc.nextLine();
            System.out.println();
            switch(volba){
                case "1":
                    createTeam(i,team);
                    i++;
                    break;
                case "2":

                    if(checkNumberOfTeam(team,check) == true){
                        matchesControlUi.matchesControl(champ, sc);
                        volba="4";
                    }else{System.out.println("Neni registrovano dostatek tymu!\n");}
                    break;
                case "3":
                    champ.printTeams();
                    break;
                case "4":
                    System.out.println("ukoncuji program");
                    break;
                default:
                    System.out.println("neplatna volba! pouze cisla podle menu ");
                    break;
            }
        }
    }

    public void createTeam(int i, Team[] team){
        if(i < team.length){
        System.out.print("Zadej nazev " + i + ". tymu:");
        String teamName = sc.nextLine();
        team[i] = new Team(teamName, i+1);
        champ.register(team[i]);
        }else{
            System.out.println("Prekocen zadany pocet tymu!");
        }
    }
    public boolean checkNumberOfTeam(Team[] team, boolean check){
        int counter = 0;
        for(int i = 0; i<team.length; i++){
            if(team[i] != null){
                counter += 1;
            }
        }
        if(counter >= 2){
            check = true;
        }return check;
    }
}
