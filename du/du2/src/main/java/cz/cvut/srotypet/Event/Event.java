package cz.cvut.srotypet.event;

import java.util.Date;

public class Event {

    private Date date;
    private int time;
    public Status status;
    public IdTeam idTeam;

    public Event(Date date, int time, Status status, IdTeam idTeam) {
        this.date = date;
        this.time = time;
        this.status = status;
        this.idTeam = idTeam;
    }

    public String printMatch(){
        String log = " " +
                "Den: " + date +
                " cas: " + time +
                " team:" + idTeam + "\t" +
                " udelali=" + status +
                "\n";
        return log;
    }

    @Override
    public String toString() {
        return " " +
                "Den: " + date +
                " cas: " + time +
                " team:" + idTeam + "\t" +
                " udelali=" + status +
                "\n";
    }
}
