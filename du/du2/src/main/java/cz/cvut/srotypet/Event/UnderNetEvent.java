package cz.cvut.srotypet.event;

import java.util.Date;

public class UnderNetEvent extends Event {

    public UnderNetEvent(Date date, int time, Status status, IdTeam idTeam){
        super(date, time, status, idTeam);
    }

    @Override
    public String printMatch() {
        return "Tym " + idTeam + " hral pod sit\n";
    }
}
