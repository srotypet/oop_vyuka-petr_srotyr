package cz.cvut.srotypet.championship;

public class Team {
    private String teamName;
    private Integer teamId;
    private Integer teamScore = 0;

    public Team(String teamName, Integer teamId) {
        this.teamName = teamName;
        this.teamId = teamId;
        this.teamScore = 0;
    }

    public String getTeamName() {
        return teamName;
    }

    public Integer getTeamScore() {
        return teamScore;
    }

    public Integer getTeamId() { return teamId; }

    public void setTeamScore(Integer teamScore) {
        this.teamScore += teamScore;
    }

    @Override
    public String toString() {
        return "Tym " +
                teamName + '\'';
    }
}
