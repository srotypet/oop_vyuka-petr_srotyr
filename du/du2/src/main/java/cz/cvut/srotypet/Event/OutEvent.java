package cz.cvut.srotypet.event;

import java.util.Date;

public class OutEvent extends Event {
    public OutEvent(Date date, int time, Status status, IdTeam idTeam){
        super(date, time, status, idTeam);
    }

    @Override
    public String printMatch() {
        return "Tym " + idTeam + " dal out\n";
    }
}
