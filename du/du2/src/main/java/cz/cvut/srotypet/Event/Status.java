package cz.cvut.srotypet.event;

public enum Status {
    BALL,
    START,
    STOP,
    NET,
    UNDERNET,
    OUT
}
