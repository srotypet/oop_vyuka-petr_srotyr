package cz.cvut.srotypet;

public class Main {
    public static void main(String[] args){
        System.out.println("\n//////Sarnabal championship///////");
        System.out.println("==================================\n");
        //Vytvoreni tymu
        Team t1 = new Team("Ocasomuti",1);
        Team t2 = new Team("Zizalomuti",2);
        Team t3 = new Team("Konomuti",3);
        Team t4 = new Team("Bezsamponi",4);

        //registrace tymu do zapasu
        Championship championship = new Championship();
        championship.register(t1);
        championship.register(t2);
        championship.register(t3);
        championship.register(t4);
        //System.out.println(championship);

        //Generovani zapasu
        championship.generateMatches();
        for (Match match: championship.getMatches()) {
            championship.simulatchamp(match);
        }
        //volani vypisovacich metod do konzole
        championship.printMatch();
        championship.printScore();
        championship.printPoints();
        championship.printWiener();
    }
}
