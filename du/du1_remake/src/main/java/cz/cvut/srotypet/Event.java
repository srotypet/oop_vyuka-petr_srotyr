package cz.cvut.srotypet;

import java.util.Date;

public class Event {
    private Date date;
    private int time;
    public Status status;
    public IdTeam idTeam;

    public Event(Date date, int time, Status status, IdTeam idTeam) {
        this.date = date;
        this.time = time;
        this.status = status;
        this.idTeam = idTeam;
    }

    @Override
    public String toString() {
        return " " +
                "Den: " + date +
                " cas: " + time +
                " team:" + idTeam + "\t" +
                " udelali=" + status +
                "\n";
    }
}