package cz.cvut.srotypet;

import java.util.Arrays;
import java.util.Scanner;

public class Playground {

    int i =0;
    //Team[] team = new Team[20];
    public Team[] createTeams(int i, Team[] team){
        Scanner sc = new Scanner(System.in);
        System.out.print("Zadej nazev tymu:");
        String teamName = sc.nextLine();
        team[i]=new Team(teamName, i);
        i++;

        /*Team t1 = new Team("Ocasove",1);
        Team t2 = new Team("Zizalomuti",2);
        Team t3 = new Team("Konomuti",3);
        Team t4 = new Team("Bezsamponi",4);

        Championshit championshit = new Championshit();
        championshit.register(t1);
        championshit.register(t2);
        championshit.register(t3);
        championshit.register(t4);
        System.out.println(championshit);*/
        return team;
    }

    public void showTeams(Team[] teams){

       for(int j=0; j<teams.length; j++){
            System.out.println(teams[j]);}
    }
    public void registerTeams(Team[] teams, Championshit championshit){
        if (teams.length>2){
        for(int i=0; i< teams.length; i++)
        {
            championshit.register(teams[i]);
        }
        }else{
            System.out.println("Na spusteni zampionu neni dostatek tymu");
            return;
        }
        //System.out.println(championshit);
        championshit.generateMatches();
    }
    public void generateMatches(){
        new Championshit().generateMatches();
    }
}
