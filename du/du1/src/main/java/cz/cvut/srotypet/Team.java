package cz.cvut.srotypet;

public class Team {
    private String teamName;
    private Integer teamId;
    public Integer teamScore;

    public Team(String teamName, Integer teamId){
        this.teamName = teamName;
        this.teamId = teamId;
        this.teamScore = 0;
    }

    @Override
    public String toString() {
        return "Team{" +
                "teamName='" + teamName + '\'' +
                ", teamId=" + teamId +
                ", teamScore=" + teamScore +
                '}';
    }
}
