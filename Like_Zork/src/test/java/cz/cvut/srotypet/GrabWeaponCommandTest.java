package cz.cvut.srotypet;

import cz.cvut.srotypet.command.Command;
import cz.cvut.srotypet.command.GrabCommand;
import cz.cvut.srotypet.command.GrabWeaponCommand;
import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.enemy.FlyweightEnemy;
import cz.cvut.srotypet.game.GameDataImplement;
import cz.cvut.srotypet.game.RoomImplement;
import cz.cvut.srotypet.item.Potion;
import cz.cvut.srotypet.item.Weapon;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class GrabWeaponCommandTest {
    private Map<String, Command> commands = new HashMap<>();
    GrabWeaponCommand grabWeapon = new GrabWeaponCommand(commands);
    private RoomImplement room = new RoomImplement("testRoom", "test room with enemy and weapon");
    private GameDataImplement data = new GameDataImplement();

    @Test
    public void grabNullWeapon(){
        data.setCurrentRoom(room);
        String[] line = new String[2];
        line[0] = "nasad";

        String testText = grabWeapon.execute(line, data);
        Assert.assertEquals("V mistnosti neni pouzitelna zbran",testText);
    }

    @Test
    public void grabWeaponWithEnemyInRoom(){
        //RoomImplement room = new RoomImplement("testRoom", "test room with Enemy and potion");
        Weapon testWeapon = new Weapon().setName("testWeapon").setDamageMin(1).setDamageMax(3);
        FlyweightEnemy weaker = new FlyweightEnemy(10,10);
        Enemy testEnemy = Enemy.builder(weaker).setName("testEnemy").setDamageMin(1).setDamageMax(2).build();
        data.setCurrentRoom(room);
        data.getCurrentRoom().setWeapon(testWeapon);
        data.getCurrentRoom().insertEnemy(testEnemy);
        String[] line = new String[2];
        line[0] = "nasad";

        String testText = grabWeapon.execute(line, data);
        Assert.assertEquals("\n" +
                "POZOR! Nepřítel v mistnosti tě nenechá se vyzbrojit a zaútočil na tebe za: 2\n" +
                "test room with enemy and weapon\n" +
                "V místnosti se nachází:\n" +
                "( Zbraň: testWeapon,  útok: 1 - 3 )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- nepřítel: testEnemy hp: 10 -\n" +
                "- Hero: Nameless hp: 98 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }

    @Test
    public void grabWeaponWithoutEnemyInRoom(){
        Weapon testWeapon = new Weapon().setName("testWeapon").setDamageMin(1).setDamageMax(3);
        FlyweightEnemy weaker = new FlyweightEnemy(10,10);
        Enemy testEnemy = Enemy.builder(weaker).setName("testEnemy").setDamageMin(1).setDamageMax(2).build();
        data.setCurrentRoom(room);
        data.getCurrentRoom().setWeapon(testWeapon);
        String[] line = new String[2];
        line[0] = "nasad";

        String testText = grabWeapon.execute(line, data);
        Assert.assertEquals("sebrana zbran testWeapon\n" +
                "test room with enemy and weapon\n" +
                "V místnosti se nachází:\n" +
                "( Zbraň: Paraty,  útok: 0 - 5 )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 100 ( Zbran: testWeapon ( 1 - 3 ) -\n" +
                "===========================",testText);
    }
}
