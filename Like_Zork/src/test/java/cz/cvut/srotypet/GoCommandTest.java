package cz.cvut.srotypet;

import cz.cvut.srotypet.command.Command;
import cz.cvut.srotypet.command.GoCommand;
import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.enemy.FlyweightEnemy;
import cz.cvut.srotypet.game.GameDataImplement;
import cz.cvut.srotypet.game.RoomImplement;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class GoCommandTest {
    private Map<String, Command> commands = new HashMap<>();
    GoCommand go = new GoCommand(commands);
    private RoomImplement room = new RoomImplement("testRoom", "test room");
    private RoomImplement leftRoom = new RoomImplement("leftTestRoom", "Left test room");
    private RoomImplement lockRoom = new RoomImplement("LockRoom", "Locked room");
    private GameDataImplement data = new GameDataImplement();
    private FlyweightEnemy weaker = new FlyweightEnemy(10,10);
    private Enemy testEnemy = Enemy.builder(weaker).setName("testEnemy").setDamageMin(1).setDamageMax(2).build();

    @Test
    public void goToNullRoomWithoutExitsInCurrentRoom(){
        data.setCurrentRoom(room);
        String[] line = new String[2];
        line[0] = "jdi";
        line[1] =  "nekam";
        String testText = go.execute(line,data);

        Assert.assertEquals("Zadan neplatny vychod dostupne vychody:  Nejsou zde zadne vychody!",testText);

    }

    @Test
    public void goToRoomWithRegisteredExitInCurrentRoom(){
        data.setCurrentRoom(room);
        data.getCurrentRoom().registerExit(leftRoom);
        String[] line = new String[2];
        line[0] = "jdi";
        line[1] =  "nekam";
        String testText = go.execute(line,data);

        Assert.assertEquals("Zadan neplatny vychod dostupne vychody: [leftTestRoom]",testText);
    }

    @Test
    public void goToExistInCurrentRoom(){
        data.setCurrentRoom(room);
        data.getCurrentRoom().registerExit(leftRoom);
        String[] line = new String[2];
        line[0] = "jdi";
        line[1] =  "leftTestRoom";
        String testText = go.execute(line,data);

        Assert.assertEquals("Left test room\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 100 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }

    @Test
    public void goToExistWithEnemyInCurrentRoom(){
        data.setCurrentRoom(room);
        data.getCurrentRoom().registerExit(leftRoom);
        data.getCurrentRoom().insertEnemy(testEnemy);
        String[] line = new String[2];
        line[0] = "jdi";
        line[1] =  "leftTestRoom";
        String testText = go.execute(line,data);

        Assert.assertEquals("\n" +
                "POZOR! Nepřítel v mistnosti tě při odchodu napadl za: 2\n" +
                "\n" +
                "Left test room\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 98 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }

    @Test
    public void goToLockRoom(){
        data.setCurrentRoom(room);
        lockRoom.lockRoom();
        data.getCurrentRoom().registerExit(lockRoom);
        String[] line = new String[2];
        line[0] = "jdi";
        line[1] =  "LockRoom";
        String testText = go.execute(line,data);

        Assert.assertEquals("Mistnost je zamčená! Musíš najít klíč k jejímu odemčení.\n" +
                "test room\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  )\n" +
                " Dostupné východy: [LockRoom]\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -- Hero: Nameless hp: 100 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }

    @Test
    public void UnlockRoom(){
        data.setCurrentRoom(room);
        lockRoom.lockRoom();
        data.getCurrentRoom().registerExit(lockRoom);
        data.getHero().setKey();
        String[] line = new String[2];
        line[0] = "jdi";
        line[1] =  "LockRoom";
        String testText = go.execute(line,data);

        Assert.assertEquals("mistnost odemcena!",testText);
    }
}
