package cz.cvut.srotypet;

import cz.cvut.srotypet.command.AttackCommand;
import cz.cvut.srotypet.command.Command;
import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.enemy.FlyweightEnemy;
import cz.cvut.srotypet.game.GameDataImplement;
import cz.cvut.srotypet.game.RoomImplement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class AttackCommandTest {
    private Map<String, Command> commands = new HashMap<>();
    AttackCommand attack = new AttackCommand(commands);
    private RoomImplement room = new RoomImplement("testRoom", "test room with Enemy");
    private GameDataImplement data = new GameDataImplement();
    private FlyweightEnemy weaker = new FlyweightEnemy(10,10);
    private Enemy testEnemy = Enemy.builder(weaker).setName("testEnemy").setDamageMin(1).setDamageMax(1).build();
    private Enemy testEnemyWithKey = Enemy.builder(weaker).setName("Grimer").setDamageMin(1).setDamageMax(1).build();
    private Enemy testEnemyBoss = Enemy.builder(weaker).setName("Vládce portálů").setDamageMin(1).setDamageMax(1).build();
    private Enemy testOPEnemy = Enemy.builder(weaker).setName("OPEnemy").setDamageMin(199).setDamageMax(200).build();


    @Test
    public void fightWithNullEnemy(){
        String[] line = new String[2];
        line[0] = "utok";

        data.setCurrentRoom(room);
        data.getCurrentRoom().insertEnemy(null);
        String testText = attack.execute(line, data);

        Assert.assertEquals("neni na koho zaútočit",testText);
    }

    @Test
    public void fightWithEnemy_EnemyReturnAttack(){
        String[] line = new String[2];
        line[0] = "utok";

        data.getHero().setDamageMin(1);
        data.getHero().setDamageMax(2);
        data.setCurrentRoom(room);
        data.getCurrentRoom().insertEnemy(testEnemy);
        String testText = attack.execute(line, data);

        Assert.assertEquals("\n" +
                "Zaútočil jsi za: 2\n" +
                "Nepřítel zaútočil za: 2\n" +
                "===========================\n" +
                "- nepřítel: testEnemy hp: 8 -\n" +
                "- Hero: Nameless hp: 98 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }

    @Test
    public void fightWithEnemy_EnemyFall(){
        String[] line = new String[2];
        line[0] = "utok";

        data.getHero().setDamageMin(50);
        data.getHero().setDamageMax(50);
        data.setCurrentRoom(room);
        data.getCurrentRoom().insertEnemy(testEnemy);
        String testText = attack.execute(line, data);

        Assert.assertEquals("\n" +
                "Zaútočil jsi za: 50\n" +
                "\n" +
                "Nepritel porazen\n" +
                "\n" +
                "test room with Enemy\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 100 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }

    @Test
    public void fightWithEnemy_EnemyWithKey(){
        String[] line = new String[2];
        line[0] = "utok";

        data.getHero().setDamageMin(50);
        data.getHero().setDamageMax(50);
        data.setCurrentRoom(room);
        data.getCurrentRoom().insertEnemy(testEnemyWithKey);
        String testText = attack.execute(line, data);

        Assert.assertEquals("\n" +
                "Zaútočil jsi za: 50\n" +
                "Našel jsi malý klíček\n" +
                "Nepritel porazen\n" +
                "\n" +
                "test room with Enemy\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 100 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }
    @Test
    public void fightWithEnemy_EnemyBossFall() {
        String[] line = new String[2];
        line[0] = "utok";

        data.getHero().setDamageMin(50);
        data.getHero().setDamageMax(50);
        data.setCurrentRoom(room);
        data.getCurrentRoom().insertEnemy(testEnemyBoss);
        String testText = attack.execute(line, data);

        Assert.assertEquals("zabit boss", testText);
    }

    @Test
    public void fightWithEnemy_HeroFall() {
        String[] line = new String[2];
        line[0] = "utok";

        data.getHero().setDamageMin(1);
        data.getHero().setDamageMax(1);
        data.setCurrentRoom(room);
        data.getCurrentRoom().insertEnemy(testOPEnemy);
        String testText = attack.execute(line, data);

        Assert.assertEquals("\n" +
                "Zaútočil jsi za: 1\n" +
                "Nepřítel zaútočil za: 200\n" +
                "byl jsi zabit", testText);
    }
}
