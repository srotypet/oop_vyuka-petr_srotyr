package cz.cvut.srotypet;

import cz.cvut.srotypet.backpack.Backpack;
import cz.cvut.srotypet.command.Command;
import cz.cvut.srotypet.command.GrabCommand;
import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.enemy.FlyweightEnemy;
import cz.cvut.srotypet.game.GameDataImplement;
import cz.cvut.srotypet.game.RoomImplement;
import cz.cvut.srotypet.item.Potion;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;

public class GrabCommandTest {
    private Map<String, Command> commands = new HashMap<>();
    GrabCommand grab = new GrabCommand(commands);
    private RoomImplement room = new RoomImplement("testRoom", "test room with Enemy and potion");
    private GameDataImplement data = new GameDataImplement();

    @Test
    public void addPotionToInventory(){
        Potion potion = new Potion("napoj", 50);
        Backpack backpack = new Backpack(2);
        data.setCurrentRoom(room);
        data.getCurrentRoom().setPotion(potion);
        data.getHero().setPotionBag(backpack);

        String[] line = new String[2];
        line[0] = "seber";

        String testText = grab.execute(line,data);
        Assert.assertEquals("\n" +
                "test room with Enemy and potion\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 100 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);

    }
    @Test
    public void insertPotionOverCapacity(){
        Potion potion = new Potion("napoj", 50);
        Backpack backpack = new Backpack(2);

        backpack.addPotion(potion);
        backpack.addPotion(potion);
        backpack.addPotion(potion);

    }

    @Test
    public void grabWithEnemyInRoom(){
        //RoomImplement room = new RoomImplement("testRoom", "test room with Enemy and potion");
        Potion potion = new Potion("napoj", 10);
        FlyweightEnemy weaker = new FlyweightEnemy(10,10);
        Enemy testEnemy = Enemy.builder(weaker).setName("testEnemy").setDamageMin(1).setDamageMax(2).build();
        data.setCurrentRoom(room);
        data.getCurrentRoom().setPotion(potion);
        data.getCurrentRoom().insertEnemy(testEnemy);
        String[] line = new String[2];
        line[0] = "seber";
        line[1] = "neco";

        String testText = grab.execute(line, data);
        Assert.assertEquals("\n" +
                "POZOR! Nepřítel v mistnosti tě nenechá nic sebrat a zaútočil na tebe za: 2\n" +
                "test room with Enemy and potion\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "( Lektvar: napoj ) Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- nepřítel: weaker hp: 10 -\n" +
                "- Hero: Nameless hp: 98 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }
}
