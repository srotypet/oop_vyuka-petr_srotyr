package cz.cvut.srotypet;

import cz.cvut.srotypet.command.Command;
import cz.cvut.srotypet.command.UseCommand;
import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.enemy.FlyweightEnemy;
import cz.cvut.srotypet.game.GameDataImplement;
import cz.cvut.srotypet.game.RoomImplement;
import cz.cvut.srotypet.item.Potion;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class UseCommandTest {
    private Map<String, Command> commands = new HashMap<>();
    UseCommand use = new UseCommand(commands);
    private RoomImplement room = new RoomImplement("testRoom", "test room with enemy and weapon");
    private GameDataImplement data = new GameDataImplement();
    private Potion testPotion = new Potion("testPotion",20);
    private FlyweightEnemy weaker = new FlyweightEnemy(10,10);
    private Enemy testEnemy = Enemy.builder(weaker).setName("testEnemy").setDamageMin(1).setDamageMax(2).build();


    @Test
    public void useNullPotion(){
        data.setCurrentRoom(room);
        String[] line = new String[2];
        line[0] = "pouzij";
        line[1] = "testPotion";

        String testText = use.execute(line, data);
        Assert.assertEquals("\n\nZadan neplatny lektvar lektvary v inventari: []",testText);
    }

    @Test
    public void usePotionWithEnemyInRoom(){
        room.insertEnemy(testEnemy);
        data.setCurrentRoom(room);
        data.getHero().addPotion(testPotion);
        String[] line = new String[2];
        line[0] = "pouzij";
        line[1] = "testPotion";

        String testText = use.execute(line, data);
        Assert.assertEquals("\n" +
                "POZOR! Nepřítel v mistnosti tě nenechá požít lektvar zadarmo a zaútočil na tebe za: 2\n" +
                "test room with enemy and weapon\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- nepřítel: testEnemy hp: 10 -\n" +
                "- Hero: Nameless hp: 118 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }


    @Test
    public void useWrongPotion(){
        data.setCurrentRoom(room);
        data.getHero().addPotion(testPotion);
        String[] line = new String[2];
        line[0] = "pouzij";
        line[1] = "Potion";

        String testText = use.execute(line, data);
        Assert.assertEquals("\n\nZadan neplatny lektvar lektvary v inventari: [testPotion]",testText);
    }

    @Test
    public void useWithoutSecondParameter(){
        data.setCurrentRoom(room);
        data.getHero().addPotion(testPotion);
        String[] line = new String[1];
        line[0] = "pouzij";

        String testText = use.execute(line, data);
        Assert.assertEquals("\n\nNezadano co ma byt pouzito. V inventari jsou: [testPotion]",testText);
    }

    @Test
    public void usePotion(){
        data.setCurrentRoom(room);
        data.getHero().addPotion(testPotion);
        String[] line = new String[2];
        line[0] = "pouzij";
        line[1] = "testPotion";

        String testText = use.execute(line, data);
        Assert.assertEquals("\n" +
                "\n" +
                "test room with enemy and weapon\n" +
                "V místnosti se nachází:\n" +
                "(  )\n" +
                "(  ) \n" +
                "Nejsou zde zadne vychody!\n" +
                "===========================\n" +
                "- Nepřítel nepřítomen -\n" +
                "- Hero: Nameless hp: 120 ( Zbran: Paraty ( 0 - 5 ) -\n" +
                "===========================",testText);
    }
}
