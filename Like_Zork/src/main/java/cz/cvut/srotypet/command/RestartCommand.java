package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;

import java.util.Map;

public class RestartCommand implements Command {
    private Map<String, Command> commands;

    public RestartCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "restart";
    }

    @Override
    public String execute(String[] line, GameData gameData) {
        gameData.createGame();
        return "Restartuji hru¨\n " + gameData.getCurrentRoom().getDescriptionWithExits() + gameData.getHero().getHeroDescript();
    }
}
