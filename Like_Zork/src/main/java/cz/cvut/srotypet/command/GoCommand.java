package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;
import cz.cvut.srotypet.game.Room;

import java.util.Map;
import java.util.Random;

public class GoCommand implements Command {
    private Map<String, Command> commands;
    private Random rand = new Random();

    public GoCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "jdi";
    }

    @Override
    public String execute(String[] line, GameData gameData) {
        Room currentRoom = gameData.getCurrentRoom();

        if(line.length > 1){
            Room room = gameData.getCurrentRoom().getExitByName(line[1]);
            if(room != null) {
                if(gameData.getCurrentRoom().getEnemy() != null){
                    int enemyAttack = gameData.getCurrentRoom().getEnemy().getDamageMin() + rand.nextInt(gameData.getCurrentRoom().getEnemy().getDamageMax() - gameData.getCurrentRoom().getEnemy().getDamageMin()) + 1;
                    gameData.getHero().hurtHealth(enemyAttack);
                    gameData.setCurrentRoom(room);
                    return "\nPOZOR! Nepřítel v mistnosti tě při odchodu napadl za: " + enemyAttack + "\n\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
                }
                if(room.lockedRoom()) {
                    if (gameData.getHero().getKey()) {
                        room.unlockRoom();
                        return "mistnost odemcena!";
                    }
                    if (!gameData.getHero().getKey()) {
                        //System.out.println(room.getLock());
                        return room.getLock() + "\n" + currentRoom.getDescriptionWithExits() + gameData.getHero().getHeroDescript();
                    }
                }
                gameData.setCurrentRoom(room);
                return room.getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
            }else {
                //gameData.setCurrentRoom(currentRoom);
                return "Zadan neplatny vychod dostupne vychody: " + currentRoom.getAvailExits();
            }
        }
        return "Nezadano kam jit dostupne vychody: " + currentRoom.getAvailExits();
    }
}
