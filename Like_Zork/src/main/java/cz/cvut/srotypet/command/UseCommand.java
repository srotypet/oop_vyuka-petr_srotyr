package cz.cvut.srotypet.command;

import cz.cvut.srotypet.backpack.Backpack;
import cz.cvut.srotypet.game.GameData;
import cz.cvut.srotypet.game.Room;
import cz.cvut.srotypet.item.Potion;
import cz.cvut.srotypet.item.Weapon;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

public class UseCommand implements Command {
    private Map<String, Command> commands;
    private Random rand = new Random();
    private String enemyReact = "";

    public UseCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "pouzij";
    }

    @Override
    public String execute(String[] line, GameData gameData)
    {
        if(gameData.getCurrentRoom().getEnemy() != null){
            int enemyAttack = gameData.getCurrentRoom().getEnemy().getDamageMin() + rand.nextInt(gameData.getCurrentRoom().getEnemy().getDamageMax() - gameData.getCurrentRoom().getEnemy().getDamageMin()) + 1;
            gameData.getHero().hurtHealth(enemyAttack);
            enemyReact = "POZOR! Nepřítel v mistnosti tě nenechá požít lektvar zadarmo a zaútočil na tebe za: " + enemyAttack;
        }
        if(line.length > 1){
            ArrayList<Potion> potions = gameData.getHero().getPotions();
            int size = potions.size();
            for(int i=0; i<size; i++){
                if(potions.get(i).getName().equals(line[1])){
                    gameData.getHero().healing(potions.get(i).getBoost());
                    gameData.getHero().getPotionBag().removePotion(i);
                    return "\n"+ enemyReact + "\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
                }
            }
            return "\n"+ enemyReact + "\nZadan neplatny lektvar lektvary v inventari: " + gameData.getHero().getPotionBagContent();
            }
        return "\n"+ enemyReact + "\nNezadano co ma byt pouzito. V inventari jsou: " + gameData.getHero().getPotionBagContent();
    }
}
