package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;

import java.util.Map;

public class DropCommand  implements Command{
    private Map<String, Command> commands;

    public DropCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "poloz";
    }

    @Override
    public String execute(String[] line, GameData gameData) {
        return "Poloz a lez";
    }
}
