package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;

public interface Command {

    String getName();
    String execute(String[] line, GameData gameData); // vrat zpet String Line (String[] line, GameData gamedata)
}
