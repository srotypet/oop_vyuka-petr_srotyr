package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;
import cz.cvut.srotypet.game.Room;
import cz.cvut.srotypet.item.Weapon;

import java.util.Map;
import java.util.Random;

public class GrabWeaponCommand implements Command {
    private Map<String, Command> commands;
    private Random rand = new Random();
    private String grabWeapon = "";
    public GrabWeaponCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "nasad";
    }

    @Override
    public String execute(String[] line, GameData gameData)
    {
        if(gameData.getCurrentRoom().getEnemy() != null)
        {
            int enemyAttack = gameData.getCurrentRoom().getEnemy().getDamageMin() + rand.nextInt(gameData.getCurrentRoom().getEnemy().getDamageMax() - gameData.getCurrentRoom().getEnemy().getDamageMin()) + 1;
            gameData.getHero().hurtHealth(enemyAttack);
            return "\nPOZOR! Nepřítel v mistnosti tě nenechá se vyzbrojit a zaútočil na tebe za: " + enemyAttack + "\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
        }
        Weapon currentWeapon = gameData.getCurrentRoom().getWeapon();
        if(gameData.getCurrentRoom().getWeapon() != null)
        {
            gameData.getCurrentRoom().setWeapon(gameData.getHero().getWeapon());
            gameData.getHero().setWeapon(currentWeapon);
            grabWeapon = "sebrana zbran " + currentWeapon.getName();
        }else
            {
            return "V mistnosti neni pouzitelna zbran";
            }
        return grabWeapon + "\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
    }
}
