package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;

import java.util.Map;

public class ExitCommand implements Command{
    private Map<String, Command> commands;

    public ExitCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "konec";
    }

    @Override
    public String execute(String[] line, GameData gameData) {
        gameData.setFinished(true);
        return "Ukonciji hru ";
    }
}
