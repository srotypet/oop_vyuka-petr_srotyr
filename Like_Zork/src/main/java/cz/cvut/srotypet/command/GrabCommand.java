package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;
import cz.cvut.srotypet.game.Room;
import cz.cvut.srotypet.item.Potion;

import java.util.Map;
import java.util.Random;

public class GrabCommand implements Command {
    private Map<String, Command> commands;
    private Random rand = new Random();

    public GrabCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() { return "seber"; }

    @Override
    public String execute(String[] line, GameData gameData) {
    if(gameData.getCurrentRoom().getEnemy() != null){
        int enemyAttack = gameData.getCurrentRoom().getEnemy().getDamageMin() + rand.nextInt(gameData.getCurrentRoom().getEnemy().getDamageMax() - gameData.getCurrentRoom().getEnemy().getDamageMin()) + 1;
        gameData.getHero().hurtHealth(enemyAttack);
        return "\nPOZOR! Nepřítel v mistnosti tě nenechá nic sebrat a zaútočil na tebe za: " + enemyAttack + "\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
    }if(gameData.getCurrentRoom().getPotion() != null)
        {
            Potion potion = gameData.getCurrentRoom().getPotion();
            gameData.getHero().addPotion(potion);
            if(gameData.getHero().getPotionBag().checkCapacity())
            {
                gameData.getCurrentRoom().removePotion();
            }
        }else{ return "Neni zde zadny lektvar"; }
    return "\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
    }
}
