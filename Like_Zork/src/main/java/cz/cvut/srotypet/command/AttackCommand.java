package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;

import java.util.Map;
import java.util.Random;

public class AttackCommand implements Command {
    private Map<String, Command> commands;
    private Random rand = new Random();

    public AttackCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "utok";
    }

    @Override
    public String execute(String[] line, GameData gameData) {
        int heroAttack = 0;
        int enemyAttack = 0;
        String keyText = "";
        if(gameData.getCurrentRoom().getEnemy() != null){
            heroAttack = gameData.getHero().getDamageMin() + rand.nextInt((gameData.getHero().getDamageMax()-gameData.getHero().getDamageMin())+1);
            gameData.getCurrentRoom().getEnemy().getHurt(heroAttack);
            //System.out.println("\nZaútočil jsi za: " + heroAttack);
            if(gameData.getCurrentRoom().getEnemy().getHealth() > 0)
            {
                enemyAttack =gameData.getCurrentRoom().getEnemy().getDamageMin() + rand.nextInt(gameData.getCurrentRoom().getEnemy().getDamageMax() - gameData.getCurrentRoom().getEnemy().getDamageMin()) + 1;
                gameData.getHero().hurtHealth(enemyAttack);
                //System.out.println("Nepřítel zaútočil za: " + enemyAttack);
                if(gameData.getHero().getHealth() < 0){
                    gameData.getHero().heroDied();
                    gameData.setFinished(true);
                    return "\nZaútočil jsi za: " + heroAttack + "\nNepřítel zaútočil za: " + enemyAttack + "\nbyl jsi zabit";
                }
            }if(gameData.getCurrentRoom().getEnemy().getHealth() <= 0){
                gameData.getCurrentRoom().setWeapon(gameData.getCurrentRoom().getEnemy().getDropWeapon());
                if (gameData.getCurrentRoom().getEnemy().getName().equals("Grimer")){
                    gameData.getHero().setKey();
                    keyText = "Našel jsi malý klíček";
                    //System.out.println("Našel jsi malý klíček");
                }if(gameData.getCurrentRoom().getEnemy().getName().equals("Vládce portálů")){
                    gameData.bossDied();
                    gameData.getCurrentRoom().withdrawEnemy();
                    gameData.setFinished(true);
                    return "zabit boss";
                }
                gameData.getCurrentRoom().withdrawEnemy();
                return "\nZaútočil jsi za: " + heroAttack + "\n" + keyText +"\nNepritel porazen\n\n" + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
            }
        }if(gameData.getCurrentRoom().getEnemy() == null){
            return "neni na koho zaútočit";
        }return "\nZaútočil jsi za: " + heroAttack + "\nNepřítel zaútočil za: " + enemyAttack + "\n" + gameData.getCurrentRoom().getEnemyInfo() + "\n" +  gameData.getHero().getHeroDescript();
    }
}
