package cz.cvut.srotypet.command;

import cz.cvut.srotypet.game.GameData;

import java.util.Map;

public class HelpCommand implements Command {

    private Map<String, Command> commands;

    public HelpCommand(Map<String, Command> commands){
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String execute(String[] line, GameData gameData) {
        return "Dostupné příkazy: " + commands.keySet().toString();
    }
}
