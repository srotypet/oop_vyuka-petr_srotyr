package cz.cvut.srotypet.backpack;

import cz.cvut.srotypet.item.Potion;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Queue;

public class Backpack extends Observable {
    private final int capacity;
    //private Queue<Potion> potions = new LinkedList<>();
    private final ArrayList<Potion> potions = new ArrayList<>();

    void notificat(String notifikace){
        setChanged();
        notifyObservers(notifikace);
    }

    public Backpack(int capacity) {
        this.capacity = capacity;
        //potions = new Potion[capacity];
    }

    public void addPotion(Potion potion) {
        if (potions.size() < capacity) {
            potions.add(potion);
            notificat("lektvar přidán na opasek");
        } else
            System.out.println("Plný opasek");
    }

    public boolean checkCapacity(){
        if(potions.size() < capacity){
            return true;
        }return false;
    }

    public void removePotion(int i) {
        if (potions != null) {
            potions.remove(i);
            notificat("lektvar požit a lahev zahozena (kupodivu se po dopadu vypařila)");
        } else
            System.out.println("Prazndy opasek");
    }

    public ArrayList<Potion> getPotions(){ return potions; }

    public String getContent(){ return potions.toString(); }

}