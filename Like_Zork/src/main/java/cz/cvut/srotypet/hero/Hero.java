package cz.cvut.srotypet.hero;

import cz.cvut.srotypet.backpack.Backpack;
import cz.cvut.srotypet.item.Key;
import cz.cvut.srotypet.item.Potion;
import cz.cvut.srotypet.item.Weapon;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Hero implements Observer {
    private String name;
    private int health;
    private int damageMin;
    private int damageMax;
    private int defend;
    private Weapon weapon;
    boolean key = false;
    private Backpack potionBag; // = new Backpack(5);
    private boolean heroDead = false;

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(((String)arg).toString());
    }

    public Hero(String name){
        this.name = name;
        health = 100;
        defend = 0;
    }

    public void setPotionBag(Backpack potionBag){
        this.potionBag = potionBag;
    }

    public String getHeroDescript(){
        return
                "- Hero: " + name + " hp: " + health + " " + getWeaponDescript() + " -\n"
                + "===========================";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public String getWeaponDescript(){
        //String descript = "( Zbran: " + weapon.getName() + " ( " + weapon.getDamageMin() + " - " + weapon.getDamageMax() + " )";
        return "( Zbran: " + weapon.getName() + " ( " + weapon.getDamageMin() + " - " + weapon.getDamageMax() + " )";
    }

    public void setWeapon(Weapon weapon){
        this.weapon = weapon;
        setDamageMin(weapon.getDamageMin());
        setDamageMax(weapon.getDamageMax());
    }

    public Weapon getWeapon(){ return weapon; }
    public void healing(int healing) {
        this.health = health + healing;
    }
    public void hurtHealth(int hurt){ this.health = health - hurt;}

    public int getHealth() { return health; }

    public void setHealth(int health) { this.health = health; }

    public int getDamageMin() {
        return damageMin;
    }

    public void setDamageMin(int damage1) {
        this.damageMin = damage1;
    }

    public int getDamageMax() { return damageMax; }

    public void setDamageMax(int damage2) {
        this.damageMax = damage2;
    }

    public int getDefend() {
        return defend;
    }

    public void setDefend(int defend) {
        this.defend = defend;
    }

    public void heroDied(){ heroDead = true; }

    public boolean getHeroDied(){ return heroDead; }

    public void addPotion(Potion potion){ potionBag.addPotion(potion); }

    public String getPotionBagContent(){ return potionBag.getContent(); }

    public ArrayList<Potion> getPotions(){return potionBag.getPotions(); }

    public Backpack getPotionBag(){ return  potionBag; }

    public void setKey(){ key = true; }
    public boolean getKey(){ return key; }


}
