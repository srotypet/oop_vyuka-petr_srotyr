package cz.cvut.srotypet.game;

import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.hero.Hero;
import cz.cvut.srotypet.item.Key;
import cz.cvut.srotypet.item.Potion;
import cz.cvut.srotypet.item.Weapon;

import java.util.*;

public class RoomImplement implements Room{

    private final String name;
    private final String description;
    private final Map<String,Room> exits = new HashMap<>();
    private Key key;
    boolean locked = false;
    private Enemy enemy;
    private Weapon weapon;
    private Potion potion;
    private boolean enemyNotFall;

    public RoomImplement(String name, String description){
        this.name = name;
        this.description = description;
    }

    /**
     *  Adds new exit to map
     */
    @Override
    public void registerExit(Room room){
        exits.put(room.getName(), room);
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     *  Method returns description of this room (from getDescription call)
     *  and should add possible exit names
     */
    @Override
    public String getDescriptionWithExits() {
        if(exits.isEmpty()){
            return getDescription() + " \nNejsou zde zadne vychody!" + "\n" + getEnemyInfo();
        }
        List<String> rooms = new ArrayList<>();
        for(Room room : getExits()){
            rooms.add(room.getName());
        }
        return getDescription() + "\n Dostupné východy: " + rooms.toString() + "\n" + getEnemyInfo();
    }

    /**
     *  Method returns description of this room
     */
    @Override
    public String getDescription() {
        return description + "\nV místnosti se nachází:\n" + "( " +  getAvailWeapon() + " )\n" + "( " + getAvailPotion() + " )";
    }

    /**
     *  Return unmodifiable view of our map
     */
    @Override
    public Collection<Room> getExits() {
        return Collections.unmodifiableCollection(exits.values());
    }

    /**
     *  Returns room based on entered room (exit) name
     */
    @Override
    public Room getExitByName(String name) {
        return exits.getOrDefault(name, null);
    }

    @Override
    public void insertEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    @Override
    public void withdrawEnemy() {
        this.enemy = null;
    }

    @Override
    public Enemy getEnemy() {
        return enemy;
    }

    @Override
    public String getEnemyInfo(){
        if(enemy != null){
            return "===========================\n"
                    + "- nepřítel: " + enemy.getName() + " hp: " + enemy.getHealth() + " -";
        }else
            return "===========================\n" +"- Nepřítel nepřítomen -";
    }

    @Override
    public boolean enemyNotFall() {
        if(enemy != null)
            enemyNotFall = true;
        else
            enemyNotFall = false;
        return  enemyNotFall;
    }

    @Override
    public boolean lockedRoom() {
        if(locked)
            return true;
        else
            return false;
    }

    @Override
    public void lockRoom() {
        locked = true;
    }

    @Override
    public void unlockRoom() {
        locked = false;
    }

    public String getLock(){
        if(locked)
        {
            return "Mistnost je zamčená! Musíš najít klíč k jejímu odemčení.";
        }
        return "Mistnost je odemčená!";
    }

    @Override
    public String getAvailExits(){

        if(exits.isEmpty()){
            return " Nejsou zde zadne vychody!";
        }
        List<String> rooms = new ArrayList<>();
        for(Room room : getExits()){
            rooms.add(room.getName());
        }
        return rooms.toString();
    }

    @Override
    public void setWeapon(Weapon weapon) { this.weapon = weapon; }

    @Override
    public String getAvailWeapon() {
        if(weapon != null)
        {
            return "Zbraň: " + weapon.getName() + ",  útok: " + weapon.getDamageMin() + " - " + weapon.getDamageMax();
        }return "";
    }

    @Override
    public Weapon getWeapon() { return weapon; }

    @Override
    public void setPotion(Potion potion) { this.potion = potion; }

    @Override
    public String getAvailPotion() {
        if (potion != null){
            return "Lektvar: " + potion.getName();
        }return "";
    }

    @Override
    public Potion getPotion() { return potion; }

    @Override
    public void removePotion() {
        this.potion = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomImplement room = (RoomImplement) o;
        return Objects.equals(name, room.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
