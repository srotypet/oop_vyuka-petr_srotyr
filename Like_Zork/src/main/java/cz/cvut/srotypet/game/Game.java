package cz.cvut.srotypet.game;

public interface Game {
    String welcomeMessage();
    String endMessage();
    //String deathMessage();
    boolean isFinished();

    String processTextCommand(String line);
}
