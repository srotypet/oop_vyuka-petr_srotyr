package cz.cvut.srotypet.game;

import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.item.Potion;
import cz.cvut.srotypet.item.Weapon;

import java.security.Key;
import java.util.Collection;

public interface Room {

    String getName();
    String getDescription();
    String getDescriptionWithExits();
    Collection<Room> getExits();
    Room getExitByName(String name);
    void registerExit(Room room);

    void insertEnemy(Enemy enemy);
    void withdrawEnemy();
    Enemy getEnemy();
    boolean enemyNotFall();
    String getEnemyInfo();

    String getAvailExits();

    boolean lockedRoom();
    void lockRoom();
    void unlockRoom();
    String getLock();

    void setWeapon(Weapon weapon);
    String getAvailWeapon();
    Weapon getWeapon();

    void setPotion(Potion potion);
    String getAvailPotion();
    Potion getPotion();
    void removePotion();
}
