package cz.cvut.srotypet.game;

import cz.cvut.srotypet.backpack.Backpack;
import cz.cvut.srotypet.enemy.Enemy;
import cz.cvut.srotypet.enemy.FlyweightEnemy;
import cz.cvut.srotypet.hero.Hero;
import cz.cvut.srotypet.item.Potion;
import cz.cvut.srotypet.item.Weapon;

public class GameDataImplement implements GameData {

    private Room currentRoom;
    private boolean finished = false;
    private final Hero hero = new Hero("Nameless");
    private boolean bossDied = false;
    //private Backpack potionBag = new Backpack(5);

    /**
     *  Room map registration in constructor
     */
    public GameDataImplement(){
        createGame();
    }

    @Override
    public void createGame(){
        Backpack potionBag = new Backpack(5);
        potionBag.addObserver(hero);
        hero.setPotionBag(potionBag);
        hero.setHealth(100);
        //Tvorba místností
        Room startRoom = new RoomImplement("Vesnice", "Stojíš ve zpustlé vesnici před tajuplným portálem. Z dálky slyšíš slabé: Legendy hovoří, že objekt za portálem je začarovaný a dobrodruzi se zde často ztrácí a hynó. \nNa portálu je napsáno: Vstup do země bludů, špatných vtipů a portálu. Zkřiž své síly s pánem portalu!");
        Room darkCaveBottom = new RoomImplement("zavetri", "Prostor kde už tě nesejme vítr, zato si ale dej bacha na interiérové větry");
        Room darkCaveBottomL = new RoomImplement("vlevosatna", "Odlož sve levé brnění, které nemáš. Nic se ti nestane!");
        Room darkCaveBottomR = new RoomImplement("vpravokolarna", "Zaparkuj bejka! ");
        Room lightCaveCenter = new RoomImplement("vstupnihala", "Před sebou cítíš východ dovnitř");
        Room darkCaveTopL = new RoomImplement("chodbal", "Blinkr doprava, švih ho doleva");
        Room darkCaveTopR = new RoomImplement("chodbap", "Blinkr doleva, švih ho doprava");
        Room lightCaveL = new RoomImplement("skladryb", "Je čerstvá! anebo ne?");
        Room lightCaveR = new RoomImplement("neco", "Někde");
        Room meadowL = new RoomImplement("skladkurat", "Dej si battle s kohoutem");
        Room meadowR = new RoomImplement("meziprostor", "Prostor mezi prostorem a prostorem");
        Room grove = new RoomImplement("atrium", "A + triumf, zapózuj si!");
        Room crossroadL = new RoomImplement("zasobovocihala", "Hodiny odbijí půlnoc, ale nikde nikdo"); //krom enemaka ktery oidmita zasobovat
        Room crossroadR = new RoomImplement("unikovachodba", "Tudy bych nezkoušel zdrhat");
        Room roadL = new RoomImplement("tunel", "Hluboký, temný a smrdí zde ponožky");
        Room roadR = new RoomImplement("cekarna", "... a teď? ... ... a teď? ... ... a teď? ... ... a teď? ... ... a teď? ... ... a nic! ");
        Room village = new RoomImplement("foaje", "Zapomněl jsem si trenýrky, do háje!");
        Room forestL = new RoomImplement("pripravna", "Jsi ready?");
        Room forestR = new RoomImplement("strojovna", "Promazal jsi závity?");
        Room deepForestL = new RoomImplement("mycka", "Vzrly dvere, vrzly v pantu, prišla parta buz... rakeťáků");
        Room deepForestR = new RoomImplement("dilna", "Vždy tě udělaj!");
        Room darkForestL = new RoomImplement("tridirna", "Prvý, druhý, třetí, čtvrtý... a kámen zase chybí co?");
        Room darkForestR = new RoomImplement("skladiste", "Bordel jako u mě doma");
        Room highPlateauL = new RoomImplement("kuchyne", "Kuchař to tam sype jak Babica");
        Room highPlateauR = new RoomImplement("WC", "Winter (is) Coming ");
        Room tipL = new RoomImplement("jidelna", "Fakt tady jsou vařené stonožky...");
        Room tipR = new RoomImplement("vylevka", "Točíš se jak hovno v sifónu");
        Room despairRoad = new RoomImplement("vytah", "Bezfuškotah ovšem mimo provoz, jdi po šikmině se zubama");
        Room despairPath = new RoomImplement("schodiste", "Eskalatory rozbité, musíš po svých");
        Room despairCrossroad = new RoomImplement("hala", "Skoro mi tam dala");
        Room graveyard = new RoomImplement("hrbitov", "250 Kč za noc s polopenzí");
        Room graveyardKeeperLair = new RoomImplement("marnice", "Sem tam tě praští tvárnice");
        Room bossRoom = new RoomImplement("kanclvedouciho", "Sedni ke stolu a hraj se šéfem kamennou tvář");

        this.currentRoom = startRoom;
        //Registrace východů pro místnosti
        startRoom.registerExit(darkCaveBottom);

        darkCaveBottom.registerExit(darkCaveBottomL);
        darkCaveBottom.registerExit(darkCaveBottomR);
        darkCaveBottom.registerExit(lightCaveCenter);

        darkCaveBottomL.registerExit(darkCaveBottom);
        darkCaveBottomL.registerExit(darkCaveTopL);
        darkCaveBottomL.registerExit(darkCaveBottomR);
        darkCaveBottomL.registerExit(darkCaveTopR);

        darkCaveBottomR.registerExit(darkCaveBottom);
        darkCaveBottomR.registerExit(darkCaveTopR);
        darkCaveBottomR.registerExit(darkCaveBottomL);
        darkCaveBottomR.registerExit(darkCaveTopL);

        lightCaveL.registerExit(meadowL);
        lightCaveL.registerExit(darkCaveTopL);
        lightCaveL.registerExit(lightCaveR);

        darkCaveTopL.registerExit(darkCaveBottomL);
        darkCaveTopL.registerExit(lightCaveL);
        darkCaveTopL.registerExit(lightCaveCenter);
        darkCaveTopL.registerExit(darkCaveBottomR);

        lightCaveCenter.registerExit(darkCaveBottom);
        lightCaveCenter.registerExit(darkCaveTopL);
        lightCaveCenter.registerExit(darkCaveTopR);
        lightCaveCenter.registerExit(grove);

        darkCaveTopR.registerExit(darkCaveBottomR);
        darkCaveTopR.registerExit(lightCaveR);
        darkCaveTopR.registerExit(lightCaveCenter);
        darkCaveTopR.registerExit(darkCaveBottomL);

        lightCaveR.registerExit(meadowR);
        lightCaveL.registerExit(darkCaveTopR);
        lightCaveL.registerExit(lightCaveR);

        meadowL.registerExit(despairRoad);
        meadowL.registerExit(crossroadL);
        meadowL.registerExit(deepForestL);
        meadowL.registerExit(lightCaveL);

        grove.registerExit(village);
        grove.registerExit(graveyard);
        grove.registerExit(lightCaveCenter);
        grove.registerExit(graveyardKeeperLair);

        meadowR.registerExit(despairRoad);
        meadowR.registerExit(crossroadR);
        meadowR.registerExit(deepForestR);
        meadowR.registerExit(lightCaveR);

        crossroadL.registerExit(meadowL);
        crossroadL.registerExit(crossroadR);
        crossroadL.registerExit(forestL);
        crossroadL.registerExit(roadL);

        roadL.registerExit(crossroadL);
        roadL.registerExit(highPlateauL);
        roadL.registerExit(village);
        roadL.registerExit(forestL);

        village.registerExit(roadL);
        village.registerExit(despairRoad);
        village.registerExit(roadR);
        village.registerExit(grove);

        roadR.registerExit(crossroadR);
        roadR.registerExit(highPlateauR);
        roadR.registerExit(village);
        roadR.registerExit(forestR);

        crossroadR.registerExit(meadowR);
        crossroadR.registerExit(crossroadL);
        crossroadR.registerExit(forestR);
        crossroadR.registerExit(roadR);

        deepForestL.registerExit(darkForestL);
        deepForestL.registerExit(forestL);
        deepForestL.registerExit(meadowL);

        forestL.registerExit(deepForestL);
        forestL.registerExit(crossroadL);
        forestL.registerExit(roadL);

        forestR.registerExit(deepForestR);
        forestR.registerExit(crossroadR);
        forestR.registerExit(roadR);

        deepForestR.registerExit(darkForestR);
        deepForestR.registerExit(forestR);
        deepForestR.registerExit(meadowR);

        highPlateauL.registerExit(tipL);
        highPlateauL.registerExit(darkForestL);
        highPlateauL.registerExit(roadL);

        darkForestL.registerExit(highPlateauL);
        darkForestL.registerExit(deepForestL);

        darkForestR.registerExit(highPlateauR);
        darkForestR.registerExit(deepForestR);

        highPlateauR.registerExit(tipR);
        highPlateauR.registerExit(darkForestR);
        highPlateauR.registerExit(roadR);

        tipL.registerExit(highPlateauL);
        tipL.registerExit(tipR);

        tipR.registerExit(highPlateauR);
        tipR.registerExit(tipL);

        despairRoad.registerExit(village);
        despairRoad.registerExit(despairPath);
        despairRoad.registerExit(meadowR);
        despairRoad.registerExit(meadowL);

        despairPath.registerExit(despairRoad);
        despairPath.registerExit(despairCrossroad);

        graveyard.registerExit(despairCrossroad);
        graveyard.registerExit(grove);

        despairCrossroad.registerExit(graveyard);
        despairCrossroad.registerExit(graveyardKeeperLair);
        despairCrossroad.registerExit(despairPath);
        despairCrossroad.registerExit(bossRoom);

        graveyardKeeperLair.registerExit(despairCrossroad);
        graveyardKeeperLair.registerExit(grove);

        bossRoom.lockRoom();
        bossRoom.registerExit(despairCrossroad);

        //implementace zbrani
        Weapon claws = new Weapon().setName("Paraty").setDamageType("drasajici").setDamageMin(0).setDamageMax(5);
        Weapon stick = new Weapon().setName("klacek").setDamageType("bodne").setDamageMin(5).setDamageMax(10);
        Weapon rustySword = new Weapon().setName("zrezka").setDamageType("secne").setDamageMin(7).setDamageMax(13);
        Weapon sword1 = new Weapon().setName("jednoruční meč").setDamageType("secne").setDamageMin(10).setDamageMax(15);
        Weapon sword2 = new Weapon().setName("obouruční meč").setDamageType("secne").setDamageMin(30).setDamageMax(50);
        Weapon bastard = new Weapon().setName("meč bastard").setDamageType("secne").setDamageMin(20).setDamageMax(30);
        Weapon mace = new Weapon().setName("palcát").setDamageType("tupe").setDamageMin(15).setDamageMax(25);
        Weapon morningstar = new Weapon().setName("řemdih").setDamageType("tupe").setDamageMin(20).setDamageMax(30);
        Weapon hammer = new Weapon().setName("kladivo").setDamageType("tupe").setDamageMin(15).setDamageMax(35);
        Weapon warhammer = new Weapon().setName("válečné kladivo").setDamageType("tupe").setDamageMin(45).setDamageMax(65);
        Weapon pike = new Weapon().setName("píka").setDamageType("bodne").setDamageMin(15).setDamageMax(35);
        Weapon longstick = new Weapon().setName("tyč").setDamageType("bodne").setDamageMin(15).setDamageMax(35);
        Weapon tinyStickWithHeavyMetalHead = new Weapon().setName("Mala tyčka s železnou hlavou").setDamageType("dute").setDamageMin(65).setDamageMax(95);

        Potion smallEstusFlask = new Potion("lecivka", 50);
        Potion mediumEstusFlask = new Potion("med",75);
        Potion largeEstusFlask = new Potion("Demizon",100);
        startRoom.setPotion(smallEstusFlask);
        darkCaveBottomR.setPotion(smallEstusFlask);
        lightCaveL.setPotion(mediumEstusFlask);
        grove.setPotion(mediumEstusFlask);
        crossroadL.setPotion(mediumEstusFlask);
        village.setPotion(mediumEstusFlask);
        forestR.setPotion(mediumEstusFlask);
        darkForestR.setPotion(mediumEstusFlask);
        despairCrossroad.setPotion(largeEstusFlask);

        hero.setWeapon(claws);
        startRoom.setWeapon(stick);

        //implementace neprátel
        FlyweightEnemy weak = new FlyweightEnemy(25,0);
        FlyweightEnemy easy = new FlyweightEnemy(50,3);
        FlyweightEnemy normal = new FlyweightEnemy(75,5);
        FlyweightEnemy hard = new FlyweightEnemy(100,10);
        FlyweightEnemy extrem = new FlyweightEnemy(150,20);
        FlyweightEnemy boss = new FlyweightEnemy(250,30);

        Enemy windman = Enemy.builder(weak).setName("Větrník").setDamageMin(2).setDamageMax(5).build();
        Enemy lockerMan = Enemy.builder(easy).setName("Školnik").setDamageMin(4).setDamageMax(9).build();
        Enemy nobody = Enemy.builder(normal).setName("Nikdo").setDamageMin(6).setDamageMax(9).build();
        Enemy rooster = Enemy.builder(easy).setName("Kohout").setDamageMin(10).setDamageMax(15).build();
        Enemy interspaceMan = Enemy.builder(normal).setName("Meziprostorník").setDamageMin(6).setDamageMax(9).build();
        Enemy washmaster = Enemy.builder(easy).setName("Mistr").setDamageMin(5).setDamageMax(9).build();
        Enemy cooker = Enemy.builder(hard).setName("Kuchta").setDamageMin(8).setDamageMax(12).build();
        Enemy whiteWalker = Enemy.builder(extrem).setName("Chodec").setDamageMin(10).setDamageMax(16).build();
        Enemy eater = Enemy.builder(hard).setName("Žrout").setDamageMin(7).setDamageMax(11).build();
        Enemy muk = Enemy.builder(extrem).setName("Grimer").setDamageMin(3).setDamageMax(20).build();
        Enemy ghoul = Enemy.builder(hard).setName("Ghůl").setDamageMin(10).setDamageMax(16).build();
        Enemy morguer = Enemy.builder(extrem).setName("Spalovač").setDamageMin(15).setDamageMax(23).build();
        Enemy despairer = Enemy.builder(boss).setName("Vládce portálů").setDamageMin(20).setDamageMax(25).build();

        /*Enemy windman1 = new Enemy().setflyweightEnemy(windMan).setHealth(10).setMinAttack(2).setMaxAttack(5);
        //Enemy lockerMan1 = new Enemy().setflyweightEnemy(lockerMan).setHealth(15).setMinAttack(3).setMaxAttack(5);
        //Enemy wheeler1 = new Enemy().setflyweightEnemy(wheeler).setHealth(15).setMinAttack(4).setMaxAttack(6);
        //Enemy fisherstor1 = new Enemy().setflyweightEnemy(fisherstor).setHealth(25).setMinAttack(4).setMaxAttack(7);
        //Enemy roomer1 = new Enemy().setflyweightEnemy(roomer).setHealth(20).setMinAttack(3).setMaxAttack(7);
        //Enemy welcomer1 = new Enemy().setflyweightEnemy(welcomer).setHealth(30).setMinAttack(5).setMaxAttack(9);
        Enemy nobody1 = new Enemy().setflyweightEnemy(nobody).setHealth(40).setMinAttack(6).setMaxAttack(9);
        Enemy rooster1 = new Enemy().setflyweightEnemy(rooster).setHealth(5).setMinAttack(10).setMaxAttack(15);
        Enemy interspaceMan1 = new Enemy().setflyweightEnemy(interspaceMan).setHealth(15).setMinAttack(6).setMaxAttack(9);
        //Enemy supplyMan1 = new Enemy().setflyweightEnemy(supplyMan).setHealth(15).setMinAttack(4).setMaxAttack(8);
        //Enemy tunneler1 = new Enemy().setflyweightEnemy(tunneler).setHealth(10).setMinAttack(7).setMaxAttack(12);
        //Enemy fellowship1 = new Enemy().setflyweightEnemy(fellowship).setHealth(30).setMinAttack(8).setMaxAttack(13);
        //Enemy waiting1 = new Enemy().setflyweightEnemy(waiting).setHealth(10).setMinAttack(5).setMaxAttack(12);
        //Enemy escapist1 = new Enemy().setflyweightEnemy(escapist).setHealth(50).setMinAttack(1).setMaxAttack(5);
        Enemy washmaster1 = new Enemy().setflyweightEnemy(washmaster).setHealth(30).setMinAttack(5).setMaxAttack(9);
        //Enemy preparer1 = new Enemy().setflyweightEnemy(preparer).setHealth(15).setMinAttack(4).setMaxAttack(9);
        //Enemy tapper1 = new Enemy().setflyweightEnemy(tapper).setHealth(35).setMinAttack(6).setMaxAttack(10);
        //Enemy mister1 = new Enemy().setflyweightEnemy(mister).setHealth(45).setMinAttack(5).setMaxAttack(9);
        Enemy cooker1 = new Enemy().setflyweightEnemy(cooker).setHealth(20).setMinAttack(8).setMaxAttack(11);
        //Enemy sorter1 = new Enemy().setflyweightEnemy(sorter).setHealth(10).setMinAttack(5).setMaxAttack(9);
        //Enemy storager1 = new Enemy().setflyweightEnemy(storager).setHealth(50).setMinAttack(2).setMaxAttack(6);
        Enemy whitewalker1 = new Enemy().setflyweightEnemy(whiteWalker).setHealth(60).setMinAttack(7).setMaxAttack(12);
        Enemy eater1 = new Enemy().setflyweightEnemy(eater).setHealth(60).setMinAttack(8).setMaxAttack(11);
        Enemy muk1 = new Enemy().setflyweightEnemy(muk).setHealth(75).setMinAttack(3).setMaxAttack(10);
        //Enemy chorer1 = new Enemy().setflyweightEnemy(chorer).setHealth(15).setMinAttack(10).setMaxAttack(15);
        //Enemy bevelWithTeeth1 = new Enemy().setflyweightEnemy(bevelWithTeeth).setHealth(50).setMinAttack(6).setMaxAttack(10);
        Enemy ghoul1 = new Enemy().setflyweightEnemy(ghoul).setHealth(75).setMinAttack(10).setMaxAttack(15);
        Enemy morguer1 = new Enemy().setflyweightEnemy(morguer).setHealth(90).setMinAttack(15).setMaxAttack(19);
        Enemy despairer1 = new Enemy().setflyweightEnemy(despairer).setHealth(100).setMinAttack(20).setMaxAttack(30);
        */
        windman.setDropWeapon(rustySword);
        nobody.setDropWeapon(bastard);
        rooster.setDropWeapon(sword1);
        interspaceMan.setDropWeapon(sword2);
        washmaster.setDropWeapon(morningstar);
        cooker.setDropWeapon(pike);
        whiteWalker.setDropWeapon(warhammer);
        eater.setDropWeapon(mace);
        muk.setDropWeapon(longstick);
        ghoul.setDropWeapon(hammer);
        morguer.setDropWeapon(tinyStickWithHeavyMetalHead);

        darkCaveBottom.insertEnemy(windman);
        darkCaveBottomL.insertEnemy(lockerMan);
        //darkCaveBottomR.insertEnemy(wheeler1);
        //lightCaveL.insertEnemy(fisherstor1);
        //darkCaveTopL.insertEnemy(roomer1);
        //lightCaveCenter.insertEnemy(welcomer1);
        //darkCaveTopR.insertEnemy(roomer1);
        lightCaveR.insertEnemy(nobody);
        meadowL.insertEnemy(rooster);
        meadowR.insertEnemy(interspaceMan);
        //crossroadL.insertEnemy(supplyMan1);
        //roadL.insertEnemy(tunneler1);
        //village.insertEnemy(fellowship1);
        //roadR.insertEnemy(waiting1);
        //crossroadR.insertEnemy(escapist1);
        deepForestL.insertEnemy(washmaster);
        //forestL.insertEnemy(preparer1);
        //forestR.insertEnemy(tapper1);
        //deepForestR.insertEnemy(mister1);
        highPlateauL.insertEnemy(cooker);
        //darkForestR.insertEnemy(sorter1);
        //darkForestR.insertEnemy(storager1);
        highPlateauR.insertEnemy(whiteWalker);
        tipL.insertEnemy(eater);
        tipR.insertEnemy(muk);
        //despairRoad.insertEnemy(chorer1);
        //despairPath.insertEnemy(bevelWithTeeth1);
        graveyard.insertEnemy(ghoul);
        graveyardKeeperLair.insertEnemy(morguer);
        bossRoom.insertEnemy(despairer);
    }

    /**
     *  Sets room, where the user currently resides
     */
    @Override
    public void setCurrentRoom(Room currentRoom) { this.currentRoom = currentRoom; }

    @Override
    public Room getCurrentRoom() {
        return currentRoom;
    }

    /**
     *  Sets finished flag, indicating game is done/finished
     */
    @Override
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /**
     *  Retrieves finished flag -> parent components decides whether to end the game
     *  based on this method
     */
    @Override
    public boolean isFinished() {
        return finished;
    }

    @Override
    public void bossDied() {
        bossDied = true;
    }

    @Override
    public boolean getBossDied() {
        return bossDied;
    }

    @Override
    public Hero getHero() { return hero; }
}
