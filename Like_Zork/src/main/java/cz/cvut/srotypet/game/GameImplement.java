package cz.cvut.srotypet.game;

import cz.cvut.srotypet.command.*;

import java.util.HashMap;
import java.util.Map;

public class GameImplement implements Game {

    private Map<String, Command> commands = new HashMap<>();
    private GameData gameData;

    public GameImplement(){
        this.registerCommands();
        this.gameData = new GameDataImplement();
    }

    /**
     *  Method registering immutable Command instances
     *
     */
    private void registerCommands(){
        Command help = new HelpCommand(commands);
        Command go = new GoCommand(commands);
        Command exit = new ExitCommand(commands);
        Command restart = new RestartCommand(commands);
        Command grabWeapon = new GrabWeaponCommand(commands);
        Command attack = new AttackCommand(commands);
        //Command drop = new DropCommand(commands);
        Command use = new UseCommand(commands);
        Command grab = new GrabCommand(commands);
        commands.put(help.getName(), help);
        commands.put(go.getName(), go);
        commands.put(exit.getName(), exit);
        commands.put(restart.getName(), restart);
        commands.put(grabWeapon.getName(), grabWeapon);
        commands.put(attack.getName(), attack);
        //commands.put(drop.getName(), drop);
        commands.put(use.getName(), use);
        commands.put(grab.getName(), grab);
    }

    /**
     *  Method returns welcome message which should be printed right after
     *  game is started
     */
    @Override
    public String welcomeMessage() {
        //TODO doplnit pořádnou uvítací hlášku
        return "Hra jako Zork, \n" +
                "========================== \n" +
                "\n" + "Vitej ve hre jako Zork, \n" +
                "Dlouho dobu si se doma válel na kavalci, ale jednoho dne přišel zvrat... zjistil jsi že máš hlad. " +
                "\nVstal jsi a kouknul se do lednice, ale jediné co jsi tam našel byly 2 vejce a párek... Nu což musíš nakoupit. " +
                "\nOblíkl ses nasadil jsi si své podomácku vyrobené Wolverinovi pařáty (3 tužky slepené izolepou a oddělené korkem) a vydal se na cestu. " +
                "\nSotva si vyšel před dům uslyšel jsi hrom, a kolem tebe se zatřásla zem... najednou jsi se propadl do země. " +
                "\nProbral jsi se ve zvláštní až kreslené zemi, na obzoru jsi zahlédl koně na rytíři jak běží tvým směrem. " +
                "\nVýtej v zemi Korvůdské zapěl rytíř, nikdy jsem tě tady neviděl ty musíš být ten slavný rek zmiňovaný v proroctvích který nás zbaví bludného prokletí. " +
                "\nDál rytíř nic neřekl jen tě vyhodil uprostřed zpustlé vesnice před portálem." +
                "\npro nápovědu napiš help\n\n"
                + gameData.getCurrentRoom().getDescriptionWithExits() + "\n" + gameData.getHero().getHeroDescript();
    }

    /**
     *  Method returns end message which should be printed right after
     *  game is finished
     */
    @Override
    public String endMessage() {
        if(gameData.getHero().getHeroDied()){
            return "Hrdina byl porazen, zařadil jsi se mezi ostatní dobrodruhy, které byly naverbovány podivnou bouří. Zdejší prostory zůstávájí nadále podivuhodně pospojované temným pánem portalu ";
        }if(gameData.getBossDied()){
            return "Porazil jsi pána portalu... to je nečekané od dob vzniku těchto prostor se to zatím povedlo pouze jednomu člověku... Pánovi portalu. " +
                    "\nA to znamená..." +
                    "\nMáme nového vládce portalu blahopřeji! V další verzi této hry se staneš vládcem portalu, budeš moct verbovat hrdiny, kterým budeš trápit mozkové závity zamotaností svého světa " +
                    "\n\n KONEC HRY";
        }

        return "Utekl jsi ze hry do reálného světa! KONEC? ";
    }

    /**
     *  Method parses input line and should divide its parts to command name
     *  and array of input arguments (0-N). Based on parsed command name,
     *  specific command should be looked up and executed. If none is found,
     *  default message should be returned
     */
    @Override
    public String processTextCommand(String line) {
        String result;
        String[] splitLine = line.split(" ");
        Command command = commands.getOrDefault(splitLine[0], null);
        if(command != null){
            result = command.execute(splitLine, gameData);
        }
        else{
            result = "Neznámý příkaz, zkuste jiný nebo vyzkoušejte příkaz 'help'";
        }
        return result;
    }

    /**
     *  Method delegates its call to mutable GameData instance, which hold current game state. This
     *  state should be checked after each command evaluation a possibly terminate whole app if
     *  true is returned
     */
    @Override
    public boolean isFinished() {
        return gameData.isFinished();
    }
}
