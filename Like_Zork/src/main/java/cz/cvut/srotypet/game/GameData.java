package cz.cvut.srotypet.game;

import cz.cvut.srotypet.hero.Hero;

public interface GameData {

    void createGame();
    boolean isFinished();
    void setFinished(boolean finished);
    Room getCurrentRoom();
    void setCurrentRoom(Room currentRoom);

    Hero getHero();
    void bossDied();
    boolean getBossDied();
}
