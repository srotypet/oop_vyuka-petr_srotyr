package cz.cvut.srotypet.ui;

import cz.cvut.srotypet.game.Game;

import java.util.Scanner;

public class CommandLineUi {
    private Game game;
    private static CommandLineUi singleInstance = null;

    private CommandLineUi(Game game){
        this.game = game;
    }
    /**
     *  Ffter ui is started, application prints welcome messages and waits for user input
     *  then proceeds to process user input and return messages back here
     *  -> to be represented on this view.
     */

    //singleton s holderem (na cvikach)
    public static CommandLineUi getInstance(Game game){
        if(singleInstance == null)
            singleInstance = new CommandLineUi(game);
        return singleInstance;
    }

    public void start(){
        try(Scanner scanner = new Scanner(System.in)){
            System.out.println(this.game.welcomeMessage());
            /*System.out.print("Zadej jmeno sve postavy: ");
            //String heroName = scanner.next();*/
            while(!this.game.isFinished()){
                System.out.print("> ");
                System.out.println(this.game.processTextCommand(scanner.nextLine()));
            }
            //break z while a pridat reset (newGame this....)
            System.out.println(this.game.endMessage());
        }
    }
}
