package cz.cvut.srotypet.enemy;

import cz.cvut.srotypet.item.Weapon;

public class Enemy {
    private final int damageMin;
    private final int damageMax;
    private int health;
    private final String name;
    private FlyweightEnemy flyweightEnemy;
    private Weapon dropWeapon;

    public static EnemyBuilder builder(FlyweightEnemy flyweightEnemy) { return new EnemyBuilder(flyweightEnemy); }

    public Enemy(FlyweightEnemy enemy, String name, int damageMin, int damageMax){
        this.flyweightEnemy = enemy;
        this.name = name;
        this.damageMin = damageMin;
        this.damageMax = damageMax;
        this.health = enemy.getHealth();
    }

    public String getName(){ return name; }

    public void setDropWeapon(Weapon weapon) {
        this.dropWeapon = weapon;
    }

    public int getHealth() {
        return health;
    }

    public void getHurt(int hurting) { this.health = health - hurting; }

    public int getDamageMin() {
        return damageMin;
    }

    public int getDamageMax() {
        return damageMax;
    }

    public Weapon getDropWeapon() {
        return dropWeapon;
    }
}
