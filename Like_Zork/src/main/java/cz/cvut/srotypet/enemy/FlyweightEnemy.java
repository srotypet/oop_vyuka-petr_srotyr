package cz.cvut.srotypet.enemy;

import cz.cvut.srotypet.item.Weapon;

public class FlyweightEnemy {

    private final int health;
    private final int defend;

    public FlyweightEnemy(int health, int defend ) {
        this.health = health;
        this.defend = defend;
    }

    public int getHealth(){ return health; }

    public int getDefend(){ return defend; }
}
