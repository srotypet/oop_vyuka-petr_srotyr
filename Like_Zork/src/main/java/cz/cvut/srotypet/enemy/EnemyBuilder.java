package cz.cvut.srotypet.enemy;

public class EnemyBuilder {
    private int damageMin;
    private int damageMax;
    private String name;
    private final FlyweightEnemy flyweightEnemy;

    public EnemyBuilder(FlyweightEnemy flyweightEnemy){ this.flyweightEnemy = flyweightEnemy; }

    public EnemyBuilder setName(String name){
        this.name = name;
        return this;
    }

    public EnemyBuilder setDamageMin(int damageMin){
        this.damageMin = damageMin;
        return this;
    }

    public EnemyBuilder setDamageMax(int damageMax){
        this.damageMax = damageMax;
        return this;
    }

    public Enemy build() { return new Enemy(flyweightEnemy, name, damageMin, damageMax); }
}
