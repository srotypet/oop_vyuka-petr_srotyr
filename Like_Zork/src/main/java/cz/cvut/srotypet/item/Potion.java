package cz.cvut.srotypet.item;

public class Potion {
    private int boost;
    private String name;

    public Potion(String name, int boost){
        this.name = name;
        this.boost = boost;
    }

    public String getName() { return name; }

    public int getBoost() { return boost; }

    @Override
    public String toString() {
        return name;
    }
}
