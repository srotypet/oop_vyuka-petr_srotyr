package cz.cvut.srotypet.item;

public class Weapon {
    private String name;
    private String damageType;
    private int damageMin;
    private int damageMax;

    public String getName() {
        return name;
    }

    public Weapon setName(String name) {
        this.name = name;
        return this;
    }

    public String getDamageType() {
        return damageType;
    }

    public Weapon setDamageType(String damageType) {
        this.damageType = damageType;
        return this;
    }

    public int getDamageMin() {
        return damageMin;
    }

    public Weapon setDamageMin(int damageMin) {
        this.damageMin = damageMin;
        return this;
    }

    public int getDamageMax() {
        return damageMax;
    }

    public Weapon setDamageMax(int damageMax) {
        this.damageMax = damageMax;
        return this;
    }
}
