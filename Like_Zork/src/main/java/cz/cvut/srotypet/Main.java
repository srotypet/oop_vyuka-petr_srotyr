package cz.cvut.srotypet;

import cz.cvut.srotypet.game.GameImplement;
import cz.cvut.srotypet.item.Weapon;
import cz.cvut.srotypet.ui.CommandLineUi;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

        public static void main(String[] args) {
            CommandLineUi ui = CommandLineUi.getInstance(new GameImplement());
            ui.start();
        }
}
